# Created on Wed Mar 24 10:00:00 2021
"""

@author: mcui
"""

import copy
import json
import math
import os
import pickle
import pprint as pp
import re
import time
from math import ceil
from pathlib import Path
from typing import Tuple

import ase.io
import matplotlib
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import pylibxc
from ase import Atoms
from ase.build import molecule
from ase.calculators.aims import Aims
from ase.calculators.dftb import Dftb
from ase.data import atomic_numbers, chemical_symbols, covalent_radii
from ase.eos import EquationOfState as EOS
from ase.io import Trajectory
from ase.io.jsonio import read_json
from ase.units import Bohr, kB, kJ
from ase.utils.deltacodesdft import delta
from hotcent.atomic_dft import AtomicDFT
from hotcent.confinement import PowerConfinement
from hotcent.slako import SlaterKosterTable
from typing import List, Dict, Tuple
import multiprocessing as mp
from itertools import combinations
from math import pi, sqrt

matplotlib.use('Agg')

colors = [
    "#2470a0",  #1 muted blue
    "#ca3e47",  #2 muted red
    "#f29c2b",  #3 muted orange
    '#1f640a',  #4 muted green
    "#2ca02c",  #5 cooked asparagus green
    "#9467bd",  #6 muted purple
    "#8c564b",  #7 chestnut brown
    "#e377c2",  #8 raspberry yogurt pink
    "#7f7f7f",  #9 middle gray
    "#bcbd22",  #10 curry yellow-green
    "#17becf",  # blue-teal
    "#005555",  # fhi-green
]

markers = [
'o',#0       circle 
'v',#1       triangle_down marker
's',#2       square
'*',#3       star
'p',#4       point marker
'P',#5       pentagon marker
',',#6       pixel marker
]
linestyles = [
'-',#       solid line style
'--',#      dashed line style
'-.',#      dash-dot line style
':',#       dotted line style
(0, (3, 5, 1, 5, 1, 5)),#   dashdotdotted '- .. - .. - .. '
 (0, (5, 5)),#  dashed '-  -  -  -  -  -'
]

# https://matplotlib.org/stable/tutorials/introductory/customizing.html
poster_rcParams = {"font.size": 15, 
                     "axes.labelsize": 13,
                     "axes.titlesize": 13,
                     "axes.linewidth": 1.5, # The width of the axes lines
                     "xtick.labelsize": 12,
                     "ytick.labelsize": 12,
                     "legend.fontsize": 12,
                     "lines.markersize": 20,
                     "lines.linewidth": 2.5,  # width of lines
                     }
plt.rcParams.update(poster_rcParams)

class hotpy():
    """
    A class for DFTB parameterization and calculation!

    Attributes
    ----------
    r0_dict (Dict(str, List[float,float])): store the confinement r0 of wavefunction and density function for each element
            eg. {'H': [1.0, 2.0], 'C': [3.0, 4.0], 'O': [5.0, 6.0]}
    sigma_dict: store the confinement sigma for each element
            eg. {'H': 2.0, 'C': 2.0, 'O': 2.0}

            
    
    """
    
    def __init__(self,
                 confinement_r0: Dict[str, List[float]] = {'H': [1.0, 2.0], 'C': [3.0, 4.0], 'O': [5.0, 6.0]},
                 confinement_sigma: Dict[str, float] = {'H': 2.0, 'C': 2.0, 'O': 2.0},
                 workdir: str = './',
                 slator_p: str = './',
                 xc: str = 'GGA_X_PBE+GGA_C_PBE',
                 superposition: str = 'density',) -> None:
        """Initializes a hotpy object."""
        self.confinement_r0 = confinement_r0
        self.confinement_sigma = confinement_sigma
        self.symbols = list(confinement_r0.keys())
        self.workdir = workdir
        self.slator_p = slator_p
        self.xc = xc

        self.atoms_info_quasinano = {'H': {'name': 'hydrogen', 'atomic_number': 1, 'radii' : 0.31, 'r_quasinano' : 1.6, 'sigma_quasinano' : 2.2, 'c_rep' : 1.34877, 'mu_V' : 0.20670684, 'mu_E' : 0.03640463, 'loss' : 0.00194892, 'configuration' : '1s1', 'valence_shell' : '1s1', 'Ed' : 0.0, 'Ep' : 0.0, 'Es' : -0.238603, 'Ud' : 0.0, 'Up' : 0.0, 'Us' : 0.419731},
        'He': {'name': 'helium', 'atomic_number': 2, 'radii' : 0.28, 'r_quasinano' : 1.4, 'sigma_quasinano' : 11.4, 'c_rep' : 2.34221, 'mu_V' : 37.94686489, 'mu_E' : 0.00388078, 'loss' : 0.00388078, 'configuration' : '1s2', 'valence_shell' : '1s2', 'Ed' : 0.0, 'Ep' : 0.0, 'Es' : -0.579318, 'Ud' : 0.0, 'Up' : 0.0, 'Us' : 0.742961},
        'Li': {'name': 'lithium', 'atomic_number': 3, 'radii' : 1.28, 'r_quasinano' : 5.0, 'sigma_quasinano' : 8.2, 'c_rep' : 0.44844, 'mu_V' : 0.16627782, 'mu_E' : 0.0584718, 'loss' : 0.11239397, 'configuration' : '[He] 2s1 2p0', 'valence_shell' : '2s1 2p0', 'Ed' : 0.0, 'Ep' : -0.040054, 'Es' : -0.105624, 'Ud' : 0.0, 'Up' : 0.131681, 'Us' : 0.174131},
        'Be': {'name': 'beryllium', 'atomic_number': 4, 'radii' : 0.96, 'r_quasinano' : 3.4, 'sigma_quasinano' : 13.2, 'c_rep' : 0.1, 'mu_V' : 0.40319883, 'mu_E' : 0.2817624, 'loss' : 0.34245915, 'configuration' : '[He] 2s2 2p0', 'valence_shell' : '2s2 2p0', 'Ed' : 0.0, 'Ep' : -0.074172, 'Es' : -0.206152, 'Ud' : 0.0, 'Up' : 0.224651, 'Us' : 0.270796},
        'B': {'name': 'boron', 'atomic_number': 5, 'radii' : 0.84, 'r_quasinano' : 3.0, 'sigma_quasinano' : 10.4, 'c_rep' : 0.67649, 'mu_V' : 0.07917719, 'mu_E' : 0.33362394, 'loss' : 0.20622099, 'configuration' : '[He] 2s2 2p1', 'valence_shell' : '2s2 2p1 ', 'Ed' : 0.0, 'Ep' : -0.132547, 'Es' : -0.347026, 'Ud' : 0.0, 'Up' : 0.296157, 'Us' : 0.333879},
        'C': {'name': 'carbon', 'atomic_number': 6, 'radii' : 0.76, 'r_quasinano' : 3.2, 'sigma_quasinano' : 8.2, 'c_rep' : 0.67043, 'mu_V' : 0.00531626, 'mu_E' : 0.09077775, 'loss' : 0.04832648, 'configuration' : '[He] 2s2 2p2', 'valence_shell' : '2s2 2p2 ', 'Ed' : 0.0, 'Ep' : -0.194236, 'Es' : -0.505337, 'Ud' : 0.0, 'Up' : 0.364696, 'Us' : 0.399218},
        'N': {'name': 'nitrogen', 'atomic_number': 7, 'radii' : 0.71, 'r_quasinano' : 3.4, 'sigma_quasinano' : 13.4, 'c_rep' : 0.61934, 'mu_V' : 0.0, 'mu_E' : 0.09650281, 'loss' : 0.04825141, 'configuration' : '[He] 2s2 2p3', 'valence_shell' : '2s2 2p3 ', 'Ed' : 0.0, 'Ep' : -0.260544, 'Es' : -0.682915, 'Ud' : 0.0, 'Up' : 0.430903, 'Us' : 0.464356},
        'O': {'name': 'oxygen', 'atomic_number': 8, 'radii' : 0.66, 'r_quasinano' : 3.1, 'sigma_quasinano' : 12.4, 'c_rep' : 0.62479, 'mu_V' : 0.0, 'mu_E' : 0.06633678, 'loss' : 0.0331684, 'configuration' : '[He] 2s2 2p4', 'valence_shell' : '2s2 2p4 ', 'Ed' : 0.0, 'Ep' : -0.331865, 'Es' : -0.880592, 'Ud' : 0.0, 'Up' : 0.495405, 'Us' : 0.528922},
        'F': {'name': 'fluorine', 'atomic_number': 9, 'radii' : 0.57, 'r_quasinano' : 2.7, 'sigma_quasinano' : 10.6, 'c_rep' : 0.71679, 'mu_V' : 0.0, 'mu_E' : 0.01944105, 'loss' : 0.00972059, 'configuration' : '[He] 2s2 2p5', 'valence_shell' : '2s2 2p5', 'Ed' : 0.0, 'Ep' : -0.408337, 'Es' : -1.098828, 'Ud' : 0.0, 'Up' : 0.558631, 'Us' : 0.592918},
        'Ne': {'name': 'neon', 'atomic_number': 10, 'radii' : 0.58, 'r_quasinano' : 3.2, 'sigma_quasinano' : 15.4, 'c_rep' : 1.00317, 'mu_V' : 0.03485425, 'mu_E' : 0.00894015, 'loss' : 0.00894015, 'configuration' : '[He] 2s2 2p6', 'valence_shell' : '2s2 2p6', 'Ed' : 0.0, 'Ep' : -0.490009, 'Es' : -1.33793, 'Ud' : 0.0, 'Up' : 0.620878, 'Us' : 0.656414},
        'Na': {'name': 'sodium', 'atomic_number': 11, 'radii' : 1.66, 'r_quasinano' : 5.9, 'sigma_quasinano' : 12.6, 'c_rep' : 0.31284, 'mu_V' : 0.16033429, 'mu_E' : 0.04288416, 'loss' : 0.10159885, 'configuration' : '[Ne] 3s1 3p0', 'valence_shell' : '3s1 3p0', 'Ed' : 0.0, 'Ep' : -0.02732, 'Es' : -0.100836, 'Ud' : 0.0, 'Up' : 0.087777, 'Us' : 0.165505},
        'Mg': {'name': 'magnesium', 'atomic_number': 12, 'radii' : 1.41, 'r_quasinano' : 5.0, 'sigma_quasinano' : 6.2, 'c_rep' : 0.38027, 'mu_V' : 0.07682731, 'mu_E' : 0.07169789, 'loss' : 0.07423775, 'configuration' : '[Ne] 3s2 3p0', 'valence_shell' : '3s2 3p0', 'Ed' : 0.0, 'Ep' : -0.048877, 'Es' : -0.172918, 'Ud' : 0.0, 'Up' : 0.150727, 'Us' : 0.224983},
        'Al': {'name': 'aluminium', 'atomic_number': 13, 'radii' : 1.21, 'r_quasinano' : 5.9, 'sigma_quasinano' : 12.4, 'c_rep' : 0.55369, 'mu_V' : 0.08478745, 'mu_E' : 0.24200867, 'loss' : 0.16344422, 'configuration' : '[Ne] 3s2 3p1 3d0', 'valence_shell' : '3s2 3p1 3d0', 'Ed' : 0.116761, 'Ep' : -0.099666, 'Es' : -0.284903, 'Ud' : 0.186573, 'Up' : 0.203216, 'Us' : 0.261285},
        'Si': {'name': 'silicon', 'atomic_number': 14, 'radii' : 1.11, 'r_quasinano' : 4.4, 'sigma_quasinano' : 12.8, 'c_rep' : 0.60489, 'mu_V' : 0.04834014, 'mu_E' : 0.40224167, 'loss' : 0.22522743, 'configuration' : '[Ne] 3s2 3p2 3d0', 'valence_shell' : '3s2 3p2 3d0', 'Ed' : 0.113134, 'Ep' : -0.149976, 'Es' : -0.397349, 'Ud' : 0.196667, 'Up' : 0.247841, 'Us' : 0.300005},
        'P': {'name': 'phosphorus', 'atomic_number': 15, 'radii' : 1.07, 'r_quasinano' : 4.0, 'sigma_quasinano' : 9.6, 'c_rep' : 0.60178, 'mu_V' : 0.15671473, 'mu_E' : 0.31379388, 'loss' : 0.23520188, 'configuration' : '[Ne] 3s2 3p3 3d0', 'valence_shell' : '3s2 3p3 3d0 ', 'Ed' : 0.121111, 'Ep' : -0.202363, 'Es' : -0.513346, 'Ud' : 0.206304, 'Up' : 0.289262, 'Us' : 0.338175},
        'S': {'name': 'sulfur', 'atomic_number': 16, 'radii' : 1.05, 'r_quasinano' : 3.9, 'sigma_quasinano' : 4.6, 'c_rep' : 0.55547, 'mu_V' : 0.11084994, 'mu_E' : 0.34674148, 'loss' : 0.22878567, 'configuration' : '[Ne] 3s2 3p4 3d0', 'valence_shell' : '3s2 3p4 3d0', 'Ed' : 0.134677, 'Ep' : -0.257553, 'Es' : -0.634144, 'Ud' : 0.212922, 'Up' : 0.328724, 'Us' : 0.37561},
        'Cl': {'name': 'chlorine', 'atomic_number': 17, 'radii' : 1.02, 'r_quasinano' : 3.8, 'sigma_quasinano' : 9.0, 'c_rep' : 0.57835, 'mu_V' : 0.02014474, 'mu_E' : 0.08584106, 'loss' : 0.05299756, 'configuration' : '[Ne] 3s2 3p5 3d0', 'valence_shell' : '3s2 3p5 3d0', 'Ed' : 0.150683, 'Ep' : -0.315848, 'Es' : -0.760399, 'Ud' : 0.214242, 'Up' : 0.366885, 'Us' : 0.412418},
        'Ar': {'name': 'argon', 'atomic_number': 18, 'radii' : 1.06, 'r_quasinano' : 4.5, 'sigma_quasinano' : 15.2, 'c_rep' : 0.84094, 'mu_V' : 0.14087321, 'mu_E' : 0.06124286, 'loss' : 0.09256118, 'configuration' : '[Ne] 3s2 3p6 3d0', 'valence_shell' : '3s2 3p6 3d0', 'Ed' : 0.167583, 'Ep' : -0.377389, 'Es' : -0.892514, 'Ud' : 0.207908, 'Up' : 0.404106, 'Us' : 0.448703},
        'K': {'name': 'potassium', 'atomic_number': 19, 'radii' : 2.03, 'r_quasinano' : 6.5, 'sigma_quasinano' : 15.8, 'c_rep' : 0.46696, 'mu_V' : 0.06841112, 'mu_E' : 0.04487081, 'loss' : 0.05664628, 'configuration' : '[Ar] 4s1 4p0 3d0', 'valence_shell' : '3d0 4s1 4p0', 'Ed' : 0.030121, 'Ep' : -0.029573, 'Es' : -0.085219, 'Ud' : 0.171297, 'Up' : 0.081938, 'Us' : 0.136368},
        'Ca': {'name': 'calcium', 'atomic_number': 20, 'radii' : 1.76, 'r_quasinano' : 4.9, 'sigma_quasinano' : 13.6, 'c_rep' : 0.4989, 'mu_V' : 0.05015042, 'mu_E' : 0.23303925, 'loss' : 0.14164112, 'configuration' : '[Ar] 4s2 4p0 3d0', 'valence_shell' : '4s2 4p0 3d0', 'Ed' : -0.070887, 'Ep' : -0.051543, 'Es' : -0.138404, 'Ud' : 0.299447, 'Up' : 0.128252, 'Us' : 0.177196},
        'Sc': {'name': 'scandium', 'atomic_number': 21, 'radii' : 1.7, 'r_quasinano' : 5.1, 'sigma_quasinano' : 13.6, 'c_rep' : 0.45376, 'mu_V' : 0.0499324, 'mu_E' : 0.06824609, 'loss' : 0.05909397, 'configuration' : '[Ar] 3d1 4s2 4p0', 'valence_shell' : '3d1 4s2 4p0', 'Ed' : -0.118911, 'Ep' : -0.053913, 'Es' : -0.153708, 'Ud' : 0.32261, 'Up' : 0.137969, 'Us' : 0.189558},
        'Ti': {'name': 'titanium', 'atomic_number': 22, 'radii' : 1.6, 'r_quasinano' : 4.2, 'sigma_quasinano' : 12.0, 'c_rep' : 0.4295, 'mu_V' : 0.04220923, 'mu_E' : 0.08590747, 'loss' : 0.06391609, 'configuration' : '[Ar] 3d2 4s2 4p0', 'valence_shell' : '3d2 4s2 4p0', 'Ed' : -0.156603, 'Ep' : -0.053877, 'Es' : -0.164133, 'Ud' : 0.351019, 'Up' : 0.144515, 'Us' : 0.201341},
        'V': {'name': 'vanadium', 'atomic_number': 23, 'radii' : 1.53, 'r_quasinano' : 4.3, 'sigma_quasinano' : 13.0, 'c_rep' : 0.43275, 'mu_V' : 0.02165036, 'mu_E' : 0.12013995, 'loss' : 0.07086864, 'configuration' : '[Ar] 3d3 4s2 4p0', 'valence_shell' : '3d3 4s2 4p0', 'Ed' : -0.189894, 'Ep' : -0.053055, 'Es' : -0.172774, 'Ud' : 0.376535, 'Up' : 0.149029, 'Us' : 0.211913},
        'Cr': {'name': 'chromium', 'atomic_number': 24, 'radii' : 1.39, 'r_quasinano' : 4.7, 'sigma_quasinano' : 3.6, 'c_rep' : 0.44181, 'mu_V' : 0.01555495, 'mu_E' : 0.03959469, 'loss' : 0.02749883, 'configuration' : '[Ar] 3d5 4s1 4p0', 'valence_shell' : '3d5 4s1 4p0', 'Ed' : -0.107113, 'Ep' : -0.036319, 'Es' : -0.147221, 'Ud' : 0.31219, 'Up' : 0.123012, 'Us' : 0.200284},
        'Mn': {'name': 'manganese', 'atomic_number': 25, 'radii' : 1.39, 'r_quasinano' : 3.6, 'sigma_quasinano' : 11.6, 'c_rep' : 0.4205, 'mu_V' : 0.03923703, 'mu_E' : 0.07441108, 'loss' : 0.05680462, 'configuration' : '[Ar] 3d5 4s2 4p0', 'valence_shell' : '3d5 4s2 4p0', 'Ed' : -0.248949, 'Ep' : -0.050354, 'Es' : -0.187649, 'Ud' : 0.422038, 'Up' : 0.155087, 'Us' : 0.23074},
        'Fe': {'name': 'iron', 'atomic_number': 26, 'radii' : 1.32, 'r_quasinano' : 3.7, 'sigma_quasinano' : 11.2, 'c_rep' : 0.4384, 'mu_V' : 0.02724161, 'mu_E' : 0.04500626, 'loss' : 0.03628962, 'configuration' : '[Ar] 3d6 4s2 4p0', 'valence_shell' : '3d6 4s2 4p0', 'Ed' : -0.275927, 'Ep' : -0.048699, 'Es' : -0.19444, 'Ud' : 0.442914, 'Up' : 0.156593, 'Us' : 0.239398},
        'Co': {'name': 'cobalt', 'atomic_number': 27, 'radii' : 1.26, 'r_quasinano' : 3.3, 'sigma_quasinano' : 11.0, 'c_rep' : 0.31218, 'mu_V' : 0.04384113, 'mu_E' : 0.15030079, 'loss' : 0.09692174, 'configuration' : '[Ar] 3d7 4s2 4p0', 'valence_shell' : '3d7 4s2 4p0', 'Ed' : -0.301635, 'Ep' : -0.046909, 'Es' : -0.200975, 'Ud' : 0.462884, 'Up' : 0.157219, 'Us' : 0.24771},
        'Ni': {'name': 'nickel', 'atomic_number': 28, 'radii' : 1.24, 'r_quasinano' : 3.7, 'sigma_quasinano' : 2.2, 'c_rep' : 0.46535, 'mu_V' : 0.02445643, 'mu_E' : 0.16698025, 'loss' : 0.09564209, 'configuration' : '[Ar] 3d9 4s1 4p0', 'valence_shell' : '3d9 4s1 4p0', 'Ed' : -0.170792, 'Ep' : -0.027659, 'Es' : -0.165046, 'Ud' : 0.401436, 'Up' : 0.10618, 'Us' : 0.235429},
        'Cu': {'name': 'copper', 'atomic_number': 29, 'radii' : 1.32, 'r_quasinano' : 5.2, 'sigma_quasinano' : 2.2, 'c_rep' : 0.01, 'mu_V' : 0.1846613, 'mu_E' : 0.20893306, 'loss' : 0.19673308, 'configuration' : '[Ar] 3d10 4s1 4p0', 'valence_shell' : '3d10 4s1 4p0', 'Ed' : -0.185263, 'Ep' : -0.025621, 'Es' : -0.169347, 'Ud' : 0.42067, 'Up' : 0.097312, 'Us' : 0.243169},
        'Zn': {'name': 'zinc', 'atomic_number': 30, 'radii' : 1.22, 'r_quasinano' : 4.6, 'sigma_quasinano' : 2.2, 'c_rep' : 0.1, 'mu_V' : 0.15304891, 'mu_E' : 0.22420738, 'loss' : 0.18847869, 'configuration' : '[Ar] 3d10 4s2 4p0', 'valence_shell' : '3d10 4s2 4p0', 'Ed' : -0.372826, 'Ep' : -0.040997, 'Es' : -0.219658, 'Ud' : 0.518772, 'Up' : 0.153852, 'Us' : 0.271212},
        'Ga': {'name': 'gallium', 'atomic_number': 31, 'radii' : 1.22, 'r_quasinano' : 5.9, 'sigma_quasinano' : 8.8, 'c_rep' : 0.52955, 'mu_V' : 16.5088116, 'mu_E' : 0.42632182, 'loss' : 8.46752821, 'configuration' : '[Ar] 3d10 4s2 4p1 4d0', 'valence_shell' : '4s2 4p1 4d0', 'Ed' : 0.043096, 'Ep' : -0.094773, 'Es' : -0.328789, 'Ud' : 0.051561, 'Up' : 0.205025, 'Us' : 0.279898},
        'Ge': {'name': 'germanium', 'atomic_number': 32, 'radii' : 1.2, 'r_quasinano' : 4.5, 'sigma_quasinano' : 13.4, 'c_rep' : 0.51879, 'mu_V' : 0.03622967, 'mu_E' : 0.31527324, 'loss' : 0.17573844, 'configuration' : '[Ar] 3d10 4s2 4p2 4d0', 'valence_shell' : '4s2 4p2 4d0', 'Ed' : 0.062123, 'Ep' : -0.143136, 'Es' : -0.431044, 'Ud' : 0.101337, 'Up' : 0.240251, 'Us' : 0.304342},
        'As': {'name': 'arsenic', 'atomic_number': 33, 'radii' : 1.19, 'r_quasinano' : 4.4, 'sigma_quasinano' : 5.6, 'c_rep' : 0.5092, 'mu_V' : 0.09747912, 'mu_E' : 0.23852673, 'loss' : 0.16795001, 'configuration' : '[Ar] 3d10 4s2 4p3 4d0', 'valence_shell' : '4s2 4p3 4d0', 'Ed' : 0.078654, 'Ep' : -0.190887, 'Es' : -0.532564, 'Ud' : 0.127856, 'Up' : 0.271613, 'Us' : 0.330013},
        'Se': {'name': 'selenium', 'atomic_number': 34, 'radii' : 1.2, 'r_quasinano' : 4.5, 'sigma_quasinano' : 3.8, 'c_rep' : 0.51437, 'mu_V' : 0.04485826, 'mu_E' : 0.13883118, 'loss' : 0.09179936, 'configuration' : '[Ar] 3d10 4s2 4p4 4d0', 'valence_shell' : '4s2 4p4 4d0', 'Ed' : 0.104896, 'Ep' : -0.239256, 'Es' : -0.635202, 'Ud' : 0.165858, 'Up' : 0.300507, 'Us' : 0.355433},
        'Br': {'name': 'bromine', 'atomic_number': 35, 'radii' : 1.2, 'r_quasinano' : 4.3, 'sigma_quasinano' : 6.4, 'c_rep' : 0.5002, 'mu_V' : 0.05609419, 'mu_E' : 0.10497679, 'loss' : 0.08053795, 'configuration' : '[Ar] 3d10 4s2 4p5 4d0', 'valence_shell' : '4s2 4p5 4d0', 'Ed' : 0.126121, 'Ep' : -0.288792, 'Es' : -0.73982, 'Ud' : 0.189059, 'Up' : 0.327745, 'Us' : 0.380376},
        'Kr': {'name': 'krypton', 'atomic_number': 36, 'radii' : 1.16, 'r_quasinano' : 4.8, 'sigma_quasinano' : 15.6, 'c_rep' : 0.7257, 'mu_V' : 0.22380059, 'mu_E' : 0.08211612, 'loss' : 0.15295749, 'configuration' : '[Ar] 3d10 4s2 4p6 4d0', 'valence_shell' : '4s2 4p6 4d0', 'Ed' : 0.140945, 'Ep' : -0.339778, 'Es' : -0.846921, 'Ud' : 0.200972, 'Up' : 0.353804, 'Us' : 0.404852},
        'Rb': {'name': 'rubidium', 'atomic_number': 37, 'radii' : 2.2, 'r_quasinano' : 9.1, 'sigma_quasinano' : 4.3, 'c_rep' : 0.7257, 'mu_V' : 1.0, 'mu_E' : 1.0, 'loss' : 0.15295749, 'configuration' : '[Kr] 4d0 5s1 5p0', 'valence_shell' : '4d0 5s1 5p0', 'Ed' : 0.030672, 'Ep' : -0.027523, 'Es' : -0.081999, 'Ud' : 0.180808, 'Up' : 0.07366, 'Us' : 0.130512},
        'Sr': {'name': 'strontium', 'atomic_number': 38, 'radii' : 1.95, 'r_quasinano' : 6.9, 'sigma_quasinano' : 14.8, 'c_rep' : 0.47224, 'mu_V' : 0.04940648, 'mu_E' : 0.10007433, 'loss' : 0.07470879, 'configuration' : '[Kr] 5s2 5p0 4d0', 'valence_shell' : '5s2 5p0 4d0', 'Ed' : -0.041363, 'Ep' : -0.047197, 'Es' : -0.12957, 'Ud' : 0.234583, 'Up' : 0.115222, 'Us' : 0.164724},
        'Y': {'name': 'yttrium', 'atomic_number': 39, 'radii' : 1.9, 'r_quasinano' : 5.7, 'sigma_quasinano' : 13.6, 'c_rep' : 0.42481, 'mu_V' : 0.02179022, 'mu_E' : 0.08977917, 'loss' : 0.05577309, 'configuration' : '[Kr] 4d1 5s2 5p0', 'valence_shell' : '4d1 5s2 5p0', 'Ed' : -0.092562, 'Ep' : -0.052925, 'Es' : -0.150723, 'Ud' : 0.239393, 'Up' : 0.127903, 'Us' : 0.176814},
        'Zr': {'name': 'zirconium', 'atomic_number': 40, 'radii' : 1.75, 'r_quasinano' : 5.2, 'sigma_quasinano' : 14.0, 'c_rep' : 0.4215, 'mu_V' : 0.03296568, 'mu_E' : 0.15303059, 'loss' : 0.09299407, 'configuration' : '[Kr] 4d2 5s2 5p0', 'valence_shell' : '4d2 5s2 5p0', 'Ed' : -0.13238, 'Ep' : -0.053976, 'Es' : -0.163093, 'Ud' : 0.269067, 'Up' : 0.136205, 'Us' : 0.189428},
        'Nb': {'name': 'niobium', 'atomic_number': 41, 'radii' : 1.64, 'r_quasinano' : 5.2, 'sigma_quasinano' : 15.0, 'c_rep' : 0.4265, 'mu_V' : 0.03530165, 'mu_E' : 0.23827437, 'loss' : 0.13690259, 'configuration' : '[Kr] 4d3 5s2 5p0', 'valence_shell' : '4d3 5s2 5p0', 'Ed' : -0.170468, 'Ep' : -0.053629, 'Es' : -0.172061, 'Ud' : 0.294607, 'Up' : 0.141661, 'Us' : 0.20028},
        'Mo': {'name': 'molybdenum', 'atomic_number': 42, 'radii' : 1.54, 'r_quasinano' : 4.3, 'sigma_quasinano' : 11.6, 'c_rep' : 0.42534, 'mu_V' : 0.01364052, 'mu_E' : 0.10685285, 'loss' : 0.06021965, 'configuration' : '[Kr] 4d4 5s2 5p0', 'valence_shell' : '4d4 5s2 5p0', 'Ed' : -0.207857, 'Ep' : -0.052675, 'Es' : -0.179215, 'Ud' : 0.317562, 'Up' : 0.145599, 'Us' : 0.209759},
        'Tc': {'name': 'technetium', 'atomic_number': 43, 'radii' : 1.47, 'r_quasinano' : 4.1, 'sigma_quasinano' : 12.0, 'c_rep' : 0.43008, 'mu_V' : 0.0294428, 'mu_E' : 0.1616752, 'loss' : 0.09550762, 'configuration' : '[Kr] 4d5 5s2 5p0', 'valence_shell' : '4d5 5s2 5p0', 'Ed' : -0.244973, 'Ep' : -0.051408, 'Es' : -0.18526, 'Ud' : 0.338742, 'Up' : 0.148561, 'Us' : 0.218221},
        'Ru': {'name': 'ruthenium', 'atomic_number': 44, 'radii' : 1.46, 'r_quasinano' : 4.1, 'sigma_quasinano' : 3.8, 'c_rep' : 0.41383, 'mu_V' : 0.00735323, 'mu_E' : 0.16094969, 'loss' : 0.08421412, 'configuration' : '[Kr] 4d7 5s1 5p0', 'valence_shell' : '4d7 5s1 5p0', 'Ed' : -0.191289, 'Ep' : -0.033507, 'Es' : -0.155713, 'Ud' : 0.329726, 'Up' : 0.117901, 'Us' : 0.212289},
        'Rh': {'name': 'rhodium', 'atomic_number': 45, 'radii' : 1.42, 'r_quasinano' : 4.0, 'sigma_quasinano' : 3.4, 'c_rep' : 0.40983, 'mu_V' : 0.00519394, 'mu_E' : 0.04308463, 'loss' : 0.02420312, 'configuration' : '[Kr] 4d8 5s1 5p0', 'valence_shell' : '4d8 5s1 5p0', 'Ed' : -0.218418, 'Ep' : -0.031248, 'Es' : -0.157939, 'Ud' : 0.350167, 'Up' : 0.113146, 'Us' : 0.219321},
        'Pd': {'name': 'palladium', 'atomic_number': 46, 'radii' : 1.39, 'r_quasinano' : 4.4, 'sigma_quasinano' : 2.8, 'c_rep' : 0.41219, 'mu_V' : 0.18541646, 'mu_E' : 0.2991599, 'loss' : 0.24232551, 'configuration' : '[Kr] 4d10 5s0 5p0', 'valence_shell' : '4d9 5s1 5p0', 'Ed' : -0.245882, 'Ep' : -0.0291, 'Es' : -0.159936, 'Ud' : 0.369605, 'Up' : 0.107666, 'Us' : 0.225725},
        'Ag': {'name': 'silver', 'atomic_number': 47, 'radii' : 1.45, 'r_quasinano' : 6.5, 'sigma_quasinano' : 2.0, 'c_rep' : 0.33099, 'mu_V' : 0.07609916, 'mu_E' : 0.1518937, 'loss' : 0.11401366, 'configuration' : '[Kr] 4d10 5s1 5p0', 'valence_shell' : '4d10 5s1 5p0', 'Ed' : -0.273681, 'Ep' : -0.027061, 'Es' : -0.161777, 'Ud' : 0.388238, 'Up' : 0.099994, 'Us' : 0.231628},
        'Cd': {'name': 'cadmium', 'atomic_number': 48, 'radii' : 1.44, 'r_quasinano' : 5.4, 'sigma_quasinano' : 2.0, 'c_rep' : 0.40307, 'mu_V' : 0.08817891, 'mu_E' : 0.1613604, 'loss' : 0.12479014, 'configuration' : '[Kr] 4d10 5s2 5p0', 'valence_shell' : '4d10 5s2 5p0', 'Ed' : -0.431379, 'Ep' : -0.043481, 'Es' : -0.207892, 'Ud' : 0.430023, 'Up' : 0.150506, 'Us' : 0.251776},
        'In': {'name': 'indium', 'atomic_number': 49, 'radii' : 1.42, 'r_quasinano' : 4.8, 'sigma_quasinano' : 13.2, 'c_rep' : 0.46956, 'mu_V' : 0.11712745, 'mu_E' : 0.20517951, 'loss' : 0.16118125, 'configuration' : '[Kr] 4d10 5s2 5p1 5d0', 'valence_shell' : '5s2 5p1 5d0', 'Ed' : 0.135383, 'Ep' : -0.092539, 'Es' : -0.30165, 'Ud' : 0.156519, 'Up' : 0.189913, 'Us' : 0.257192},
        'Sn': {'name': 'tin', 'atomic_number': 50, 'radii' : 1.39, 'r_quasinano' : 4.7, 'sigma_quasinano' : 13.4, 'c_rep' : 0.48443, 'mu_V' : 0.06725906, 'mu_E' : 0.19674555, 'loss' : 0.13198857, 'configuration' : '[Kr] 4d10 5s2 5p2 5d0', 'valence_shell' : '5s2 5p2 5d0', 'Ed' : 0.125834, 'Ep' : -0.135732, 'Es' : -0.387547, 'Ud' : 0.171708, 'Up' : 0.217398, 'Us' : 0.275163},
        'Sb': {'name': 'antimony', 'atomic_number': 51, 'radii' : 1.39, 'r_quasinano' : 5.2, 'sigma_quasinano' : 3.0, 'c_rep' : 0.46877, 'mu_V' : 0.04816049, 'mu_E' : 0.21542406, 'loss' : 0.13179878, 'configuration' : '[Kr] 4d10 5s2 5p3 5d0', 'valence_shell' : '5s2 5p3 5d0', 'Ed' : 0.118556, 'Ep' : -0.177383, 'Es' : -0.471377, 'Ud' : 0.184848, 'Up' : 0.241589, 'Us' : 0.294185},
        'Te': {'name': 'tellurium', 'atomic_number': 52, 'radii' : 1.38, 'r_quasinano' : 5.2, 'sigma_quasinano' : 3.0, 'c_rep' : 0.48489, 'mu_V' : 0.04120774, 'mu_E' : 0.14669681, 'loss' : 0.09398266, 'configuration' : '[Kr] 4d10 5s2 5p4 5d0', 'valence_shell' : '5s2 5p4 5d0', 'Ed' : 0.114419, 'Ep' : -0.218721, 'Es' : -0.555062, 'Ud' : 0.195946, 'Up' : 0.263623, 'Us' : 0.313028},
        'I': {'name': 'iodine', 'atomic_number': 53, 'radii' : 1.39, 'r_quasinano' : 6.2, 'sigma_quasinano' : 2.0, 'c_rep' : 0.49361, 'mu_V' : 0.0709371, 'mu_E' : 0.12820361, 'loss' : 0.099549, 'configuration' : '[Kr] 4d10 5s2 5p5 5d0', 'valence_shell' : '5s2 5p5 5d0', 'Ed' : 0.11286, 'Ep' : -0.26033, 'Es' : -0.639523, 'Ud' : 0.206534, 'Up' : 0.284168, 'Us' : 0.33146},
        'Xe': {'name': 'xenon', 'atomic_number': 54, 'radii' : 1.4, 'r_quasinano' : 5.2, 'sigma_quasinano' : 16.2, 'c_rep' : 0.56072, 'mu_V' : 0.35579205, 'mu_E' : 0.07498539, 'loss' : 0.07498539, 'configuration' : '[Kr] 4d10 5s2 5p6 5d0', 'valence_shell' : '5s2 5p6 5d0', 'Ed' : 0.111715, 'Ep' : -0.302522, 'Es' : -0.725297, 'Ud' : 0.211949, 'Up' : 0.303641, 'Us' : 0.349484},
        'Cs': {'name': 'caesium', 'atomic_number': 55, 'radii' : 2.44, 'r_quasinano' : 10.6, 'sigma_quasinano' : 2.9, 'c_rep' : 0.56072, 'mu_V' : 1.0, 'mu_E' : 1.0, 'loss' : 0.07498539, 'configuration' : '[Xe] 5d0 6s1 6p0', 'valence_shell' : '5d0 6s1 6p0', 'Ed' : -0.007997, 'Ep' : -0.027142, 'Es' : -0.076658, 'Ud' : 0.159261, 'Up' : 0.06945, 'Us' : 0.12059},
        'Ba': {'name': 'barium', 'atomic_number': 56, 'radii' : 2.15, 'r_quasinano' : 7.7, 'sigma_quasinano' : 12.0, 'c_rep' : 0.43051, 'mu_V' : 0.13844358, 'mu_E' : 0.22818987, 'loss' : 0.18335864, 'configuration' : '[Xe] 6s2 6p0 5d0', 'valence_shell' : '6s2 6p0 5d0', 'Ed' : -0.074037, 'Ep' : -0.04568, 'Es' : -0.118676, 'Ud' : 0.199559, 'Up' : 0.105176, 'Us' : 0.149382},
        'La': {'name': 'lanthanum', 'atomic_number': 57, 'radii' : 2.07, 'r_quasinano' : 7.4, 'sigma_quasinano' : 8.6, 'c_rep' : 0.39587, 'mu_V' : 0.10198071, 'mu_E' : 0.41769924, 'loss' : 0.25989817, 'configuration' : '[Xe] 5d1 6s2 6p0', 'valence_shell' : '5d1 6s2 6p0', 'Ed' : -0.113716, 'Ep' : -0.049659, 'Es' : -0.135171, 'Ud' : 0.220941, 'Up' : 0.115479, 'Us' : 0.160718},
        'Lu': {'name': 'lutetium', 'atomic_number': 71, 'radii' : 1.87, 'r_quasinano' : 5.9, 'sigma_quasinano' : 16.4, 'c_rep' : 0.39173, 'mu_V' : 0.02774737, 'mu_E' : 0.12378526, 'loss' : 0.07577581, 'configuration' : '[Xe] 4f14 5d1 6s2 6p0', 'valence_shell' : '5d1 6s2 6p0', 'Ed' : -0.064434, 'Ep' : -0.049388, 'Es' : -0.171078, 'Ud' : 0.220882, 'Up' : 0.12648, 'Us' : 0.187365},
        'Hf': {'name': 'hafnium', 'atomic_number': 72, 'radii' : 1.75, 'r_quasinano' : 5.2, 'sigma_quasinano' : 14.8, 'c_rep' : 0.38892, 'mu_V' : 0.04292708, 'mu_E' : 0.16178323, 'loss' : 0.10238225, 'configuration' : '[Xe] 4f14 5d2 6s2 6p0', 'valence_shell' : '5d2 6s2 6p0', 'Ed' : -0.098991, 'Ep' : -0.051266, 'Es' : -0.187557, 'Ud' : 0.249246, 'Up' : 0.135605, 'Us' : 0.200526},
        'Ta': {'name': 'tantalum', 'atomic_number': 73, 'radii' : 1.7, 'r_quasinano' : 4.8, 'sigma_quasinano' : 13.8, 'c_rep' : 0.38039, 'mu_V' : 0.03308661, 'mu_E' : 0.17357727, 'loss' : 0.10336753, 'configuration' : '[Xe] 4f14 5d3 6s2 6p0', 'valence_shell' : '5d3 6s2 6p0', 'Ed' : -0.132163, 'Ep' : -0.051078, 'Es' : -0.199813, 'Ud' : 0.273105, 'Up' : 0.141193, 'Us' : 0.212539},
        'W': {'name': 'tungsten', 'atomic_number': 74, 'radii' : 1.62, 'r_quasinano' : 4.2, 'sigma_quasinano' : 8.6, 'c_rep' : 0.37771, 'mu_V' : 0.01397929, 'mu_E' : 0.06090247, 'loss' : 0.03746846, 'configuration' : '[Xe] 4f14 5d4 6s2 6p0', 'valence_shell' : '5d4 6s2 6p0', 'Ed' : -0.164874, 'Ep' : -0.049978, 'Es' : -0.209733, 'Ud' : 0.294154, 'Up' : 0.144425, 'Us' : 0.223288},
        'Re': {'name': 'rhenium', 'atomic_number': 75, 'radii' : 1.51, 'r_quasinano' : 4.2, 'sigma_quasinano' : 13.0, 'c_rep' : 0.39998, 'mu_V' : 0.03268678, 'mu_E' : 0.2064769, 'loss' : 0.11975985, 'configuration' : '[Xe] 4f14 5d5 6s2 6p0', 'valence_shell' : '5d5 6s2 6p0', 'Ed' : -0.197477, 'Ep' : -0.048416, 'Es' : -0.218183, 'Ud' : 0.313288, 'Up' : 0.146247, 'Us' : 0.233028},
        'Os': {'name': 'osmium', 'atomic_number': 76, 'radii' : 1.44, 'r_quasinano' : 4.0, 'sigma_quasinano' : 8.0, 'c_rep' : 0.4078, 'mu_V' : 0.01046755, 'mu_E' : 0.04239733, 'loss' : 0.02669427, 'configuration' : '[Xe] 4f14 5d6 6s2 6p0', 'valence_shell' : '5d6 6s2 6p0', 'Ed' : -0.23014, 'Ep' : -0.046602, 'Es' : -0.22564, 'Ud' : 0.331031, 'Up' : 0.146335, 'Us' : 0.241981},
        'Ir': {'name': 'iridium', 'atomic_number': 77, 'radii' : 1.41, 'r_quasinano' : 3.9, 'sigma_quasinano' : 12.6, 'c_rep' : 0.40101, 'mu_V' : 0.01517241, 'mu_E' : 0.04962736, 'loss' : 0.03237974, 'configuration' : '[Xe] 4f14 5d7 6s2 6p0', 'valence_shell' : '5d7 6s2 6p0', 'Ed' : -0.262953, 'Ep' : -0.044644, 'Es' : -0.2324, 'Ud' : 0.347715, 'Up' : 0.145121, 'Us' : 0.250317},
        'Pt': {'name': 'platinum', 'atomic_number': 78, 'radii' : 1.36, 'r_quasinano' : 3.8, 'sigma_quasinano' : 12.8, 'c_rep' : 0.26766, 'mu_V' : 0.28039451, 'mu_E' : 0.61288511, 'loss' : 0.44654646, 'configuration' : '[Xe] 4f14 5d9 6s1 6p0', 'valence_shell' : '5d8 6s2 6p0', 'Ed' : -0.295967, 'Ep' : -0.042604, 'Es' : -0.238659, 'Ud' : 0.363569, 'Up' : 0.143184, 'Us' : 0.258165},
        'Au': {'name': 'gold', 'atomic_number': 79, 'radii' : 1.36, 'r_quasinano' : 4.8, 'sigma_quasinano' : 2.0, 'c_rep' : 0.31061, 'mu_V' : 0.13101981, 'mu_E' : 0.3084587, 'loss' : 0.21972299, 'configuration' : '[Xe] 4f14 5d10 6s1 6p0', 'valence_shell' : '5d10 6s1 6p0', 'Ed' : -0.252966, 'Ep' : -0.028258, 'Es' : -0.211421, 'Ud' : 0.361156, 'Up' : 0.090767, 'Us' : 0.255962},
        'Hg': {'name': 'mercury', 'atomic_number': 80, 'radii' : 1.32, 'r_quasinano' : 6.7, 'sigma_quasinano' : 2.0, 'c_rep' : 0.46626, 'mu_V' : 7.26633233, 'mu_E' : 0.21567929, 'loss' : 3.74125527, 'configuration' : '[Xe] 4f14 5d10 6s2 6p0', 'valence_shell' : '5d10 6s2 6p0', 'Ed' : -0.362705, 'Ep' : -0.038408, 'Es' : -0.250189, 'Ud' : 0.393392, 'Up' : 0.134398, 'Us' : 0.272767},
        'Tl': {'name': 'thallium', 'atomic_number': 81, 'radii' : 1.45, 'r_quasinano' : 7.3, 'sigma_quasinano' : 2.14, 'c_rep' : 0.31594, 'mu_V' : 0.19106951, 'mu_E' : 0.10858305, 'loss' : 0.14977371, 'configuration' : '[Xe] 4f14 5d10 6s2 6p1 6d0', 'valence_shell' : '6s2 6p1 6d0', 'Ed' : 0.081292, 'Ep' : -0.087069, 'Es' : -0.350442, 'Ud' : 0.11952, 'Up' : 0.185496, 'Us' : 0.267448},
        'Pb': {'name': 'lead', 'atomic_number': 82, 'radii' : 1.46, 'r_quasinano' : 5.7, 'sigma_quasinano' : 3.0, 'c_rep' : 0.45266, 'mu_V' : 0.07135868, 'mu_E' : 0.18724436, 'loss' : 0.12929906, 'configuration' : '[Xe] 4f14 5d10 6s2 6p2 6d0', 'valence_shell' : '6s2 6p2 6d0', 'Ed' : 0.072602, 'Ep' : -0.128479, 'Es' : -0.442037, 'Ud' : 0.128603, 'Up' : 0.209811, 'Us' : 0.280804},
        'Bi': {'name': 'bismuth', 'atomic_number': 83, 'radii' : 1.48, 'r_quasinano' : 5.8, 'sigma_quasinano' : 2.6, 'c_rep' : 0.1, 'mu_V' : 0.06979467, 'mu_E' : 0.29609191, 'loss' : 0.18292418, 'configuration' : '[Xe] 4f14 5d10 6s2 6p3 6d0', 'valence_shell' : '6s2 6p3 6d0', 'Ed' : 0.073863, 'Ep' : -0.1679, 'Es' : -0.531518, 'Ud' : 0.14221, 'Up' : 0.231243, 'Us' : 0.296301},
        'Po': {'name': 'polonium', 'atomic_number': 84, 'radii' : 1.4, 'r_quasinano' : 5.5, 'sigma_quasinano' : 2.2, 'c_rep' : 0.46413, 'mu_V' : 0.03850535, 'mu_E' : 0.12060923, 'loss' : 0.07956284, 'configuration' : '[Xe] 4f14 5d10 6s2 6p4 6d0', 'valence_shell' : '6s2 6p4 6d0', 'Ed' : 0.081795, 'Ep' : -0.206503, 'Es' : -0.620946, 'Ud' : 0.158136, 'Up' : 0.250546, 'Us' : 0.311976},
        'Ra': {'name': 'radium', 'atomic_number': 88, 'radii' : 2.21, 'r_quasinano' : 7.0, 'sigma_quasinano' : 14.0, 'c_rep' : 0.41761, 'mu_V' : 0.07562912, 'mu_E' : 0.1617348, 'loss' : 0.11867986, 'configuration' : '[Rn] 6d0 7s2 7p0', 'valence_shell' : '6d0 7s2 7p0', 'Ed' : -0.047857, 'Ep' : -0.037077, 'Es' : -0.120543, 'Ud' : 0.167752, 'Up' : 0.093584, 'Us' : 0.151368},
        'Th': {'name': 'thorium', 'atomic_number': 90, 'radii' : 2.06, 'r_quasinano' : 6.2, 'sigma_quasinano' : 4.4, 'c_rep' : 0.37772, 'mu_V' : 0.00022792, 'mu_E' : 0.05546738, 'loss' : 0.02784023, 'configuration' : '[Rn] 6d2 7s2 7p0', 'valence_shell' : '6d2 7s2 7p0', 'Ed' : -0.113604, 'Ep' : -0.045825, 'Es' : -0.161992, 'Ud' : 0.21198, 'Up' : 0.114896, 'Us' : 0.174221},}

        self.atoms_info = {'H': {'name': 'hydrogen', 'atomic_number': 1, 'radii': 0.31, 'r_wave': 2.121, 'r_dens': 3.676, 'c_rep': 1.0879024,'mu_V': 1.89e-06, 'mu_E': 0.00015504, 'loss': 0.00116241, 'r_quasinano': 1.6, 'sigma_quasinano': 2.2, 'configuration': '1s1', 'valence_shell':'1s1', 'Ed': 0.0, 'Ep': 0.0, 'Es': -0.2385469823, 'Ud': 0.0, 'Up': 0.0, 'Us': 0.3910661456},
        'He': {'name': 'helium', 'atomic_number': 2, 'radii': 0.28, 'r_wave': 1.12, 'r_dens': 2.099, 'c_rep': 2.34151191,'mu_V': 37.94632094, 'mu_E': 0.00388814, 'loss': 76.42343684, 'r_quasinano': 1.4, 'sigma_quasinano': 11.4, 'configuration': '1s2', 'valence_shell':'1s2', 'Ed': 0.0, 'Ep': 0.0, 'Es': -0.579323941, 'Ud': 0.0, 'Up': 0.0, 'Us': 0.8606722787},
        'Li': {'name': 'lithium', 'atomic_number': 3, 'radii': 1.28, 'r_wave': 4.377, 'r_dens': 11.209, 'c_rep': 0.50161393,'mu_V': 0.01789986, 'mu_E': 0.06434782, 'loss': 0.171727, 'r_quasinano': 5.0, 'sigma_quasinano': 8.2, 'configuration': '[He] 2s1 2p0', 'valence_shell':'2s1 2p0', 'Ed': 0.0, 'Ep': -0.0390984553, 'Es': -0.1051351603, 'Ud': 0.0, 'Up': 0.0962206356, 'Us': 0.167071405},
        'Be': {'name': 'beryllium', 'atomic_number': 4, 'radii': 0.96, 'r_wave': 2.89, 'r_dens': 12.655, 'c_rep': 0.53518136,'mu_V': 0.00362361, 'mu_E': 0.04984278, 'loss': 0.18903552, 'r_quasinano': 3.4, 'sigma_quasinano': 13.2, 'configuration': '[He] 2s2 2p0', 'valence_shell':'2s2 2p0', 'Ed': 0.0, 'Ep': -0.0739871075, 'Es': -0.2060100605, 'Ud': 0.0, 'Up': 0.1401820464, 'Us': 0.2983081406},
        'B': {'name': 'boron', 'atomic_number': 5, 'radii': 0.84, 'r_wave': 2.49, 'r_dens': 4.358, 'c_rep': 0.57730223,'mu_V': 0.04402454, 'mu_E': 0.15365754, 'loss': 0.47664745, 'r_quasinano': 3.0, 'sigma_quasinano': 10.4, 'configuration': '[He] 2s2 2p1', 'valence_shell':'2s2 2p1 ', 'Ed': 0.0, 'Ep': -0.1324309215, 'Es': -0.3467940174, 'Ud': 0.0, 'Up': 0.2885923035, 'Us': 0.3740305292},
        'C': {'name': 'carbon', 'atomic_number': 6, 'radii': 0.76, 'r_wave': 3.115, 'r_dens': 7.241, 'c_rep': 0.62576678,'mu_V': 0.00765813, 'mu_E': 0.0256917, 'loss': 0.07232197, 'r_quasinano': 3.2, 'sigma_quasinano': 8.2, 'configuration': '[He] 2s2 2p2', 'valence_shell':'2s2 2p2 ', 'Ed': 0.0, 'Ep': -0.1942835767, 'Es': -0.505137468, 'Ud': 0.0, 'Up': 0.3564814591, 'Us': 0.4456932381},
        'N': {'name': 'nitrogen', 'atomic_number': 7, 'radii': 0.71, 'r_wave': 2.349, 'r_dens': 2.538, 'c_rep': 0.53813142,'mu_V': 4.12e-06, 'mu_E': 0.00323431, 'loss': 0.00866427, 'r_quasinano': 3.4, 'sigma_quasinano': 13.4, 'configuration': '[He] 2s2 2p3', 'valence_shell':'2s2 2p3 ', 'Ed': 0.0, 'Ep': -0.260654834, 'Es': -0.6826430243, 'Ud': 0.0, 'Up': 0.4235454675, 'Us': 0.5151575311},
        'O': {'name': 'oxygen', 'atomic_number': 8, 'radii': 0.66, 'r_wave': 2.011, 'r_dens': 2.664, 'c_rep': 0.55538096,'mu_V': 4e-08, 'mu_E': 0.00190785, 'loss': 0.00504833, 'r_quasinano': 3.1, 'sigma_quasinano': 12.4, 'configuration': '[He] 2s2 2p4', 'valence_shell':'2s2 2p4 ', 'Ed': 0.0, 'Ep': -0.3320261219, 'Es': -0.8802104881, 'Ud': 0.0, 'Up': 0.4889023431, 'Us': 0.5830843799},
        'F': {'name': 'fluorine', 'atomic_number': 9, 'radii': 0.57, 'r_wave': 2.879, 'r_dens': 6.077, 'c_rep': 0.64582791,'mu_V': 2.3e-07, 'mu_E': 0.00021639, 'loss': 0.00063111, 'r_quasinano': 2.7, 'sigma_quasinano': 10.6, 'configuration': '[He] 2s2 2p5', 'valence_shell':'2s2 2p5', 'Ed': 0.0, 'Ep': -0.4085465468, 'Es': -1.0983052023, 'Ud': 0.0, 'Up': 0.5528044169, 'Us': 0.6498694167},
        'Ne': {'name': 'neon', 'atomic_number': 10, 'radii': 0.58, 'r_wave': 2.241, 'r_dens': 4.363, 'c_rep': 1.06521758,'mu_V': 16.94170291, 'mu_E': 0.00939355, 'loss': 33.93675794, 'r_quasinano': 3.2, 'sigma_quasinano': 15.4, 'configuration': '[He] 2s2 2p6', 'valence_shell':'2s2 2p6', 'Ed': 0.0, 'Ep': -0.4902664601, 'Es': -1.3372327375, 'Ud': 0.0, 'Up': 0.6969850757, 'Us': 0.7157660401},
        'Na': {'name': 'sodium', 'atomic_number': 11, 'radii': 1.66, 'r_wave': 5.01, 'r_dens': 14.114, 'c_rep': 0.5169109,'mu_V': 0.05348632, 'mu_E': 0.10653888, 'loss': 0.35197519, 'r_quasinano': 5.9, 'sigma_quasinano': 12.6, 'configuration': '[Ne] 3s1 3p0', 'valence_shell':'3s1 3p0', 'Ed': 0.0, 'Ep': -0.0255753936, 'Es': -0.1002523846, 'Ud': 0.0, 'Up': 0.0819827573, 'Us': 0.1629826113},
        'Mg': {'name': 'magnesium', 'atomic_number': 12, 'radii': 1.41, 'r_wave': 4.132, 'r_dens': 12.852, 'c_rep': 0.3562487,'mu_V': 0.02015889, 'mu_E': 0.06880304, 'loss': 0.20473886, 'r_quasinano': 5.0, 'sigma_quasinano': 6.2, 'configuration': '[Ne] 3s2 3p0 3d0', 'valence_shell':'3s2 3p0 3d0', 'Ed': 0.0831630771, 'Ep': -0.0365799672, 'Es': -0.166780912, 'Ud': 0.1018241934, 'Up': 0.1564522924, 'Us': 0.2494543298},
        'Al': {'name': 'aluminium', 'atomic_number': 13, 'radii': 1.21, 'r_wave': 3.446, 'r_dens': 10.829, 'c_rep': 0.40601172,'mu_V': 0.03415797, 'mu_E': 0.10563387, 'loss': 0.32532141, 'r_quasinano': 5.9, 'sigma_quasinano': 12.4, 'configuration': '[Ne] 3s2 3p1 3d0', 'valence_shell':'3s2 3p1 3d0', 'Ed': 0.0752525386, 'Ep': -0.0911658177, 'Es': -0.2781267416, 'Ud': 0.1072297959, 'Up': 0.2151044368, 'Us': 0.2936670528},
        'Si': {'name': 'silicon', 'atomic_number': 14, 'radii': 1.11, 'r_wave': 3.65, 'r_dens': 7.038, 'c_rep': 0.52264063,'mu_V': 0.04210737, 'mu_E': 0.0745442, 'loss': 0.24755837, 'r_quasinano': 4.4, 'sigma_quasinano': 12.8, 'configuration': '[Ne] 3s2 3p2 3d0', 'valence_shell':'3s2 3p2 3d0', 'Ed': 0.0836862587, 'Ep': -0.1434299631, 'Es': -0.3914531576, 'Ud': 0.1149360282, 'Up': 0.2553980381, 'Us': 0.3332172462},
        'P': {'name': 'phosphorus', 'atomic_number': 15, 'radii': 1.07, 'r_wave': 3.603, 'r_dens': 9.856, 'c_rep': 0.53994727,'mu_V': 0.09152524, 'mu_E': 0.17903877, 'loss': 0.58086663, 'r_quasinano': 4.0, 'sigma_quasinano': 9.6, 'configuration': '[Ne] 3s2 3p3 3d0', 'valence_shell':'3s2 3p3 3d0 ', 'Ed': 0.0691231097, 'Ep': -0.2009227589, 'Es': -0.5118030765, 'Ud': 0.0809570251, 'Up': 0.2890785532, 'Us': 0.3692321777},
        'S': {'name': 'sulfur', 'atomic_number': 16, 'radii': 1.05, 'r_wave': 3.993, 'r_dens': 6.962, 'c_rep': 0.48968227,'mu_V': 0.06229973, 'mu_E': 0.12614813, 'loss': 0.38263243, 'r_quasinano': 3.9, 'sigma_quasinano': 4.6, 'configuration': '[Ne] 3s2 3p4 3d0', 'valence_shell':'3s2 3p4 3d0', 'Ed': 0.0420272873, 'Ep': -0.2575731817, 'Es': -0.6338716051, 'Ud': 0.0425054699, 'Up': 0.3260328776, 'Us': 0.4061093404},
        'Cl': {'name': 'chlorine', 'atomic_number': 17, 'radii': 1.02, 'r_wave': 3.884, 'r_dens': 5.291, 'c_rep': 0.50301982,'mu_V': 0.00041395, 'mu_E': 0.00314875, 'loss': 0.00784499, 'r_quasinano': 3.8, 'sigma_quasinano': 9.0, 'configuration': '[Ne] 3s2 3p5 3d0', 'valence_shell':'3s2 3p5 3d0', 'Ed': 0.0446152194, 'Ep': -0.3159140189, 'Es': -0.7600797687, 'Ud': 0.0418482143, 'Up': 0.3644851331, 'Us': 0.443051614},
        'Ar': {'name': 'argon', 'atomic_number': 18, 'radii': 1.06, 'r_wave': 5.869, 'r_dens': 2.725, 'c_rep': 0.55194527,'mu_V': 0.06211132, 'mu_E': 0.00325191, 'loss': 0.2867342, 'r_quasinano': 4.5, 'sigma_quasinano': 15.2, 'configuration': '[Ne] 3s2 3p6 3d0', 'valence_shell':'3s2 3p6 3d0', 'Ed': 0.0462870422, 'Ep': -0.377493102, 'Es': -0.8921287855, 'Ud': 0.0413708609, 'Up': 0.4359044776, 'Us': 0.4794551739},
        'K': {'name': 'potassium', 'atomic_number': 19, 'radii': 2.03, 'r_wave': 8.79, 'r_dens': 16.79, 'c_rep': 0.45622844,'mu_V': 0.01072188, 'mu_E': 0.0431164, 'loss': 0.11329416, 'r_quasinano': 6.5, 'sigma_quasinano': 15.8, 'configuration': '[Ar] 3d0 4s1 4p0', 'valence_shell':'3d0 4s1 4p0', 'Ed': 0.0103036284, 'Ep': -0.0271329699, 'Es': -0.0841808849, 'Ud': 0.0553122715, 'Up': 0.0805520099, 'Us': 0.1367521969},
        'Ca': {'name': 'calcium', 'atomic_number': 20, 'radii': 1.76, 'r_wave': 5.951, 'r_dens': 7.447, 'c_rep': 0.44412359,'mu_V': 0.01570908, 'mu_E': 0.02464028, 'loss': 0.09926369, 'r_quasinano': 4.9, 'sigma_quasinano': 13.6, 'configuration': '[Ar] 4s2 4p0 3d0', 'valence_shell':'3d0 4s2 4p0', 'Ed': -0.0707710217, 'Ep': -0.0499417647, 'Es': -0.1375653681, 'Ud': 0.1529092016, 'Up': 0.1121654901, 'Us': 0.1918952767},
        'Sc': {'name': 'scandium', 'atomic_number': 21, 'radii': 1.7, 'r_wave': 4.701, 'r_dens': 6.941, 'c_rep': 0.44228025,'mu_V': 0.0113882, 'mu_E': 0.03308461, 'loss': 0.12497294, 'r_quasinano': 5.1, 'sigma_quasinano': 13.6, 'configuration': '[Ar] 3d1 4s2 4p0', 'valence_shell':'3d1 4s2 4p0', 'Ed': -0.1173852925, 'Ep': -0.0509523857, 'Es': -0.1521754614, 'Ud': 0.3174939125, 'Up': 0.1253293641, 'Us': 0.2049529236},
        'Ti': {'name': 'titanium', 'atomic_number': 22, 'radii': 1.6, 'r_wave': 4.105, 'r_dens': 7.277, 'c_rep': 0.43389405,'mu_V': 0.01057253, 'mu_E': 0.06008829, 'loss': 0.2003456, 'r_quasinano': 4.2, 'sigma_quasinano': 12.0, 'configuration': '[Ar] 3d2 4s2 4p0', 'valence_shell':'3d2 4s2 4p0', 'Ed': -0.1561122688, 'Ep': -0.0527860774, 'Es': -0.1636063178, 'Ud': 0.3439089796, 'Up': 0.119985058, 'Us': 0.2168362927},
        'V': {'name': 'vanadium', 'atomic_number': 23, 'radii': 1.53, 'r_wave': 4.904, 'r_dens': 8.399, 'c_rep': 0.42937144,'mu_V': 0.02164261, 'mu_E': 0.11172158, 'loss': 0.30050077, 'r_quasinano': 4.3, 'sigma_quasinano': 13.0, 'configuration': '[Ar] 3d3 4s2 4p0', 'valence_shell':'3d3 4s2 4p0', 'Ed': -0.1896457907, 'Ep': -0.052422979, 'Es': -0.1724797287, 'Ud': 0.3697134073, 'Up': 0.1161982292, 'Us': 0.2282533366},
        'Cr': {'name': 'chromium', 'atomic_number': 24, 'radii': 1.39, 'r_wave': 4.984, 'r_dens': 14.503, 'c_rep': 0.43706206,'mu_V': 0.00977088, 'mu_E': 0.03883728, 'loss': 0.11237648, 'r_quasinano': 4.7, 'sigma_quasinano': 3.6, 'configuration': '[Ar] 3d5 4s1 4p0', 'valence_shell':'3d5 4s1 4p0', 'Ed': -0.0999949697, 'Ep': -0.020058078, 'Es': -0.1398500413, 'Ud': 0.3246607653, 'Up': 0.143930314, 'Us': 0.2077727852},
        'Mn': {'name': 'manganese', 'atomic_number': 25, 'radii': 1.39, 'r_wave': 5.058, 'r_dens': 6.989, 'c_rep': 0.43150296,'mu_V': 0.02247399, 'mu_E': 0.15903729, 'loss': 0.40598011, 'r_quasinano': 3.6, 'sigma_quasinano': 11.6, 'configuration': '[Ar] 3d5 4s2 4p0', 'valence_shell':'3d5 4s2 4p0', 'Ed': -0.2488001058, 'Ep': -0.0498644246, 'Es': -0.1874419764, 'Ud': 0.4160794405, 'Up': 0.1122955227, 'Us': 0.249248055},
        'Fe': {'name': 'iron', 'atomic_number': 26, 'radii': 1.32, 'r_wave': 3.046, 'r_dens': 5.284, 'c_rep': 0.43249384,'mu_V': 0.00641137, 'mu_E': 0.01582396, 'loss': 0.0727577, 'r_quasinano': 3.7, 'sigma_quasinano': 11.2, 'configuration': '[Ar] 3d6 4s2 4p0', 'valence_shell':'3d6 4s2 4p0', 'Ed': -0.2758030907, 'Ep': -0.048232765, 'Es': -0.1942537555, 'Ud': 0.4370317301, 'Up': 0.1099776347, 'Us': 0.2590218846},
        'Co': {'name': 'cobalt', 'atomic_number': 27, 'radii': 1.26, 'r_wave': 4.09, 'r_dens': 8.896, 'c_rep': 0.44858082,'mu_V': 0.00631676, 'mu_E': 0.03515494, 'loss': 0.08665652, 'r_quasinano': 3.3, 'sigma_quasinano': 11.0, 'configuration': '[Ar] 3d7 4s2 4p0', 'valence_shell':'3d7 4s2 4p0', 'Ed': -0.3015311795, 'Ep': -0.0464599171, 'Es': -0.2008061357, 'Ud': 0.456941254, 'Up': 0.1073006196, 'Us': 0.2684425228},
        'Ni': {'name': 'nickel', 'atomic_number': 28, 'radii': 1.24, 'r_wave': 3.64, 'r_dens': 9.168, 'c_rep': 0.44212126,'mu_V': 0.00679119, 'mu_E': 0.01767824, 'loss': 0.05396701, 'r_quasinano': 3.7, 'sigma_quasinano': 2.2, 'configuration': '[Ar] 3d8 4s2 4p0', 'valence_shell':'3d8 4s2 4p0', 'Ed': -0.3261795857, 'Ep': -0.0445769203, 'Es': -0.2071717188, 'Ud': 0.476029357, 'Up': 0.1043791756, 'Us': 0.277565561},
        'Cu': {'name': 'copper', 'atomic_number': 29, 'radii': 1.32, 'r_wave': 2.749, 'r_dens': 12.036, 'c_rep': 0.29887037,'mu_V': 0.01819207, 'mu_E': 0.07745065, 'loss': 0.28805296, 'r_quasinano': 5.2, 'sigma_quasinano': 2.2, 'configuration': '[Ar] 3d10 4s1 4p0', 'valence_shell':'3d10 4s1 4p0', 'Ed': -0.1851306924, 'Ep': -0.0247482516, 'Es': -0.1691679514, 'Ud': 0.5095012987, 'Up': 0.0799802838, 'Us': 0.2403269013},
        'Zn': {'name': 'zinc', 'atomic_number': 30, 'radii': 1.22, 'r_wave': 3.224, 'r_dens': 10.092, 'c_rep': 0.37257744,'mu_V': 0.02789664, 'mu_E': 0.09011574, 'loss': 0.28317026, 'r_quasinano': 4.6, 'sigma_quasinano': 2.2, 'configuration': '[Ar] 3d10 4s2 4p0', 'valence_shell':'3d10 4s2 4p0', 'Ed': -0.3727670015, 'Ep': -0.0405656107, 'Es': -0.2195265068, 'Ud': 0.5956153807, 'Up': 0.0981767904, 'Us': 0.2951438824},
        'Ga': {'name': 'gallium', 'atomic_number': 31, 'radii': 1.22, 'r_wave': 3.093, 'r_dens': 12.112, 'c_rep': 0.54334793,'mu_V': 0.03779511, 'mu_E': 0.55002195, 'loss': 1.58959771, 'r_quasinano': 5.9, 'sigma_quasinano': 8.8, 'configuration': '[Ar] 3d10 4s2 4p1 4d0', 'valence_shell':'4s2 4p1 4d0', 'Ed': 0.0305899215, 'Ep': -0.0944407784, 'Es': -0.3283954774, 'Ud': 0.044906403, 'Up': 0.2005300692, 'Us': 0.3125241603},
        'Ge': {'name': 'germanium', 'atomic_number': 32, 'radii': 1.2, 'r_wave': 4.341, 'r_dens': 13.448, 'c_rep': 0.48774216,'mu_V': 0.03056106, 'mu_E': 0.05458907, 'loss': 0.21304932, 'r_quasinano': 4.5, 'sigma_quasinano': 13.4, 'configuration': '[Ar] 3d10 4s2 4p2 4d0', 'valence_shell':'4s2 4p2 4d0', 'Ed': 0.033186044, 'Ep': -0.1429796796, 'Es': -0.4307219304, 'Ud': 0.0446110456, 'Up': 0.2366906267, 'Us': 0.335549648},
        'As': {'name': 'arsenic', 'atomic_number': 33, 'radii': 1.19, 'r_wave': 3.962, 'r_dens': 9.772, 'c_rep': 0.46283387,'mu_V': 0.05702462, 'mu_E': 0.14336749, 'loss': 0.41531109, 'r_quasinano': 4.4, 'sigma_quasinano': 5.6, 'configuration': '[Ar] 3d10 4s2 4p3 4d0', 'valence_shell':'4s2 4p3 4d0', 'Ed': 0.0365709231, 'Ep': -0.1908281997, 'Es': -0.5322581448, 'Ud': 0.0438813832, 'Up': 0.2688053369, 'Us': 0.3596040873},
        'Se': {'name': 'selenium', 'atomic_number': 34, 'radii': 1.2, 'r_wave': 4.249, 'r_dens': 9.522, 'c_rep': 0.48653507,'mu_V': 0.04424168, 'mu_E': 0.10888714, 'loss': 0.30946877, 'r_quasinano': 4.5, 'sigma_quasinano': 3.8, 'configuration': '[Ar] 3d10 4s2 4p4 4d0', 'valence_shell':'4s2 4p4 4d0', 'Ed': 0.0396739815, 'Ep': -0.2392614542, 'Es': -0.6348763094, 'Ud': 0.0431766723, 'Up': 0.2982182622, 'Us': 0.3837096207},
        'Br': {'name': 'bromine', 'atomic_number': 35, 'radii': 1.2, 'r_wave': 3.948, 'r_dens': 4.259, 'c_rep': 0.4333376,'mu_V': 0.00010949, 'mu_E': 0.00494667, 'loss': 0.01360309, 'r_quasinano': 4.3, 'sigma_quasinano': 6.4, 'configuration': '[Ar] 3d10 4s2 4p5 4d0', 'valence_shell':'4s2 4p5 4d0', 'Ed': 0.0421947955, 'Ep': -0.2888463289, 'Es': -0.7394522236, 'Ud': 0.0425795059, 'Up': 0.3258135687, 'Us': 0.4076140433},
        'Kr': {'name': 'krypton', 'atomic_number': 36, 'radii': 1.16, 'r_wave': 6.202, 'r_dens': 2.584, 'c_rep': 0.61925487,'mu_V': 0.05116038, 'mu_E': 0.00262302, 'loss': 0.23036222, 'r_quasinano': 4.8, 'sigma_quasinano': 15.6, 'configuration': '[Ar] 3d10 4s2 4p6 4d0', 'valence_shell':'4s2 4p6 4d0', 'Ed': 0.0441164458, 'Ep': -0.3398732719, 'Es': -0.8464935645, 'Ud': 0.0420991136, 'Up': 0.3789640037, 'Us': 0.4312597903},
        'Rb': {'name': 'rubidium', 'atomic_number': 37, 'radii': 2.2, 'r_wave': 8.407, 'r_dens': 12.708, 'c_rep': 0.42276516,'mu_V': 0.08535171, 'mu_E': 0.05352604, 'loss': 0.29348869, 'r_quasinano': 9.1, 'sigma_quasinano': 16.8, 'configuration': '[Kr] 4d0 5s1 5p0', 'valence_shell':'4d0 5s1 5p0', 'Ed': 0.0052365503, 'Ep': -0.0246062385, 'Es': -0.080804801, 'Ud': 0.0599684826, 'Up': 0.0772673586, 'Us': 0.1317895613},
        'Sr': {'name': 'strontium', 'atomic_number': 38, 'radii': 1.95, 'r_wave': 8.775, 'r_dens': 17.443, 'c_rep': 0.44841797,'mu_V': 0.02755496, 'mu_E': 0.03786207, 'loss': 0.14528334, 'r_quasinano': 6.9, 'sigma_quasinano': 14.8, 'configuration': '[Kr] 5s2 5p0 4d0', 'valence_shell':'5s2 5p0 4d0', 'Ed': -0.0464001096, 'Ep': -0.0458117688, 'Es': -0.1288772058, 'Ud': 0.1218241635, 'Up': 0.102190559, 'Us': 0.1778408964},
        'Y': {'name': 'yttrium', 'atomic_number': 39, 'radii': 1.9, 'r_wave': 6.051, 'r_dens': 9.679, 'c_rep': 0.40893097,'mu_V': 0.00407815, 'mu_E': 0.00975763, 'loss': 0.03223047, 'r_quasinano': 5.7, 'sigma_quasinano': 13.6, 'configuration': '[Kr] 4d1 5s2 5p0', 'valence_shell':'4d1 5s2 5p0', 'Ed': -0.0918820478, 'Ep': -0.0516710217, 'Es': -0.1500723188, 'Ud': 0.2349257693, 'Up': 0.1119206499, 'Us': 0.1902524735},
        'Zr': {'name': 'zirconium', 'atomic_number': 40, 'radii': 1.75, 'r_wave': 5.081, 'r_dens': 10.068, 'c_rep': 0.41377396,'mu_V': 0.01785596, 'mu_E': 0.06537402, 'loss': 0.19552486, 'r_quasinano': 5.2, 'sigma_quasinano': 14.0, 'configuration': '[Kr] 4d2 5s2 5p0', 'valence_shell':'4d2 5s2 5p0', 'Ed': -0.1320108075, 'Ep': -0.053237108, 'Es': -0.1627253337, 'Ud': 0.2648107541, 'Up': 0.1136972097, 'Us': 0.2027397343},
        'Nb': {'name': 'niobium', 'atomic_number': 41, 'radii': 1.64, 'r_wave': 5.916, 'r_dens': 8.571, 'c_rep': 0.42023526,'mu_V': 0.02477258, 'mu_E': 0.06658806, 'loss': 0.2013698, 'r_quasinano': 5.2, 'sigma_quasinano': 15.0, 'configuration': '[Kr] 4d3 5s2 5p0', 'valence_shell':'4d3 5s2 5p0', 'Ed': -0.1701786278, 'Ep': -0.0529901026, 'Es': -0.1717571218, 'Ud': 0.291020093, 'Up': 0.1149226607, 'Us': 0.214098993},
        'Mo': {'name': 'molybdenum', 'atomic_number': 42, 'radii': 1.54, 'r_wave': 3.93, 'r_dens': 8.623, 'c_rep': 0.42190093,'mu_V': 0.00809895, 'mu_E': 0.09058578, 'loss': 0.25768669, 'r_quasinano': 4.3, 'sigma_quasinano': 11.6, 'configuration': '[Kr] 4d4 5s2 5p0', 'valence_shell':'4d4 5s2 5p0', 'Ed': -0.2076211822, 'Ep': -0.0520973047, 'Es': -0.1789520685, 'Ud': 0.3142042678, 'Up': 0.1147064941, 'Us': 0.2243712699},
        'Tc': {'name': 'technetium', 'atomic_number': 43, 'radii': 1.47, 'r_wave': 5.676, 'r_dens': 7.683, 'c_rep': 0.43347616,'mu_V': 0.02018874, 'mu_E': 0.12866516, 'loss': 0.32703293, 'r_quasinano': 4.1, 'sigma_quasinano': 12.0, 'configuration': '[Kr] 4d5 5s2 5p0', 'valence_shell':'4d5 5s2 5p0', 'Ed': -0.2447762458, 'Ep': -0.0508715776, 'Es': -0.1850251641, 'Ud': 0.3353482909, 'Up': 0.1135880266, 'Us': 0.2337324458},
        'Ru': {'name': 'ruthenium', 'atomic_number': 44, 'radii': 1.46, 'r_wave': 6.31, 'r_dens': 12.792, 'c_rep': 0.41693182,'mu_V': 0.00561782, 'mu_E': 0.0365289, 'loss': 0.09003929, 'r_quasinano': 4.1, 'sigma_quasinano': 3.8, 'configuration': '[Kr] 4d7 5s1 5p0', 'valence_shell':'4d7 5s1 5p0', 'Ed': -0.1910780499, 'Ep': -0.0326555809, 'Es': -0.1554628694, 'Ud': 0.3244246092, 'Up': 0.0909510369, 'Us': 0.2104793109},
        'Rh': {'name': 'rhodium', 'atomic_number': 45, 'radii': 1.42, 'r_wave': 4.13, 'r_dens': 17.002, 'c_rep': 0.41359957,'mu_V': 0.00375128, 'mu_E': 0.01779682, 'loss': 0.06548574, 'r_quasinano': 4.0, 'sigma_quasinano': 3.4, 'configuration': '[Kr] 4d8 5s1 5p0', 'valence_shell':'4d8 5s1 5p0', 'Ed': -0.2182304415, 'Ep': -0.0303868507, 'Es': -0.1577053491, 'Ud': 0.3447075969, 'Up': 0.0877955032, 'Us': 0.2172152978},
        'Pd': {'name': 'palladium', 'atomic_number': 46, 'radii': 1.39, 'r_wave': 2.935, 'r_dens': 13.357, 'c_rep': 0.38876485,'mu_V': 0.02302051, 'mu_E': 0.10623247, 'loss': 0.39665806, 'r_quasinano': 4.4, 'sigma_quasinano': 2.8, 'configuration': '[Kr] 4d10 5s0 5p0', 'valence_shell':'4d10 5s0 5p0', 'Ed': -0.148644726, 'Ep': -0.009837416, 'Es': -0.1217248837, 'Ud': 0.3895524524, 'Up': 0.0662553817, 'Us': 0.176202705},
        'Ag': {'name': 'silver', 'atomic_number': 47, 'radii': 1.45, 'r_wave': 3.536, 'r_dens': 11.456, 'c_rep': 0.37331576,'mu_V': 0.01638611, 'mu_E': 0.05347169, 'loss': 0.17746053, 'r_quasinano': 6.5, 'sigma_quasinano': 2.0, 'configuration': '[Kr] 4d10 5s1 5p0', 'valence_shell':'4d10 5s1 5p0', 'Ed': -0.2735250898, 'Ep': -0.0261510722, 'Es': -0.161565354, 'Ud': 0.4346190778, 'Up': 0.0821261911, 'Us': 0.2289222688},
        'Cd': {'name': 'cadmium', 'atomic_number': 48, 'radii': 1.44, 'r_wave': 4.289, 'r_dens': 12.74, 'c_rep': 0.41121095,'mu_V': 0.02093608, 'mu_E': 0.03502424, 'loss': 0.12577564, 'r_quasinano': 5.4, 'sigma_quasinano': 2.0, 'configuration': '[Kr] 4d10 5s2 5p0', 'valence_shell':'4d10 5s2 5p0', 'Ed': -0.4313019614, 'Ep': -0.0430209589, 'Es': -0.2077239331, 'Ud': 0.4727331083, 'Up': 0.102798833, 'Us': 0.2722418657},
        'In': {'name': 'indium', 'atomic_number': 49, 'radii': 1.42, 'r_wave': 3.822, 'r_dens': 14.905, 'c_rep': 0.51013574,'mu_V': 0.01439516, 'mu_E': 0.24838896, 'loss': 0.71285764, 'r_quasinano': 4.8, 'sigma_quasinano': 13.2, 'configuration': '[Kr] 4d10 5s2 5p1 5d0', 'valence_shell':'5s2 5p1 5d0', 'Ed': 0.0276237868, 'Ep': -0.0921556401, 'Es': -0.3011792115, 'Ud': 0.0460265258, 'Up': 0.1866881675, 'Us': 0.2837459748},
        'Sn': {'name': 'tin', 'atomic_number': 50, 'radii': 1.39, 'r_wave': 4.677, 'r_dens': 14.81, 'c_rep': 0.46307161,'mu_V': 0.02972399, 'mu_E': 0.06633898, 'loss': 0.23117081, 'r_quasinano': 4.7, 'sigma_quasinano': 13.4, 'configuration': '[Kr] 4d10 5s2 5p2 5d0', 'valence_shell':'5s2 5p2 5d0', 'Ed': 0.0285381991, 'Ep': -0.1355310321, 'Es': -0.3871268215, 'Ud': 0.0463207594, 'Up': 0.2148380946, 'Us': 0.3002909116},
        'Sb': {'name': 'antimony', 'atomic_number': 51, 'radii': 1.39, 'r_wave': 4.537, 'r_dens': 9.738, 'c_rep': 0.43375439,'mu_V': 0.04253684, 'mu_E': 0.11771271, 'loss': 0.33431448, 'r_quasinano': 5.2, 'sigma_quasinano': 3.0, 'configuration': '[Kr] 4d10 5s2 5p3 5d0', 'valence_shell':'5s2 5p3 5d0', 'Ed': 0.0308070901, 'Ep': -0.1772934043, 'Es': -0.47096684, 'Ud': 0.0458347102, 'Up': 0.2395789841, 'Us': 0.3178104001},
        'Te': {'name': 'tellurium', 'atomic_number': 52, 'radii': 1.38, 'r_wave': 4.722, 'r_dens': 10.556, 'c_rep': 0.46266581,'mu_V': 0.04507348, 'mu_E': 0.10632313, 'loss': 0.30828643, 'r_quasinano': 5.2, 'sigma_quasinano': 3.0, 'configuration': '[Kr] 4d10 5s2 5p4 5d0', 'valence_shell':'5s2 5p4 5d0', 'Ed': 0.0334469368, 'Ep': -0.2187086138, 'Es': -0.5546267842, 'Ud': 0.0451322146, 'Up': 0.2619817273, 'Us': 0.3354033891},
        'I': {'name': 'iodine', 'atomic_number': 53, 'radii': 1.39, 'r_wave': 5.341, 'r_dens': 9.235, 'c_rep': 0.45024538,'mu_V': 0.01368196, 'mu_E': 0.063263, 'loss': 0.15623999, 'r_quasinano': 6.2, 'sigma_quasinano': 2.0, 'configuration': '[Kr] 4d10 5s2 5p5 5d0', 'valence_shell':'5s2 5p5 5d0', 'Ed': 0.0360821672, 'Ep': -0.2603764043, 'Es': -0.6390383175, 'Ud': 0.0444222828, 'Up': 0.2827878363, 'Us': 0.3528350093},
        'Xe': {'name': 'xenon', 'atomic_number': 54, 'radii': 1.4, 'r_wave': 6.539, 'r_dens': 2.153, 'c_rep': 0.57386227,'mu_V': 0.85077204, 'mu_E': 0.00399367, 'loss': 3.30535401, 'r_quasinano': 5.2, 'sigma_quasinano': 16.2, 'configuration': '[Kr] 4d10 5s2 5p6 5d0', 'valence_shell':'5s2 5p6 5d0', 'Ed': 0.0385191299, 'Ep': -0.3026179034, 'Es': -0.7247429625, 'Ud': 0.0437845857, 'Up': 0.3227378668, 'Us': 0.3700472439},
        'Cs': {'name': 'caesium', 'atomic_number': 55, 'radii': 2.44, 'r_wave': 9.975, 'r_dens': 18.757, 'c_rep': 0.45615921,'mu_V': 0.08435958, 'mu_E': 0.09544232, 'loss': 0.36452187, 'r_quasinano': 10.6, 'sigma_quasinano': 13.6, 'configuration': '[Xe] 5d0 6s1 6p0', 'valence_shell':'5d0 6s1 6p0', 'Ed': -0.0117249964, 'Ep': -0.023626155, 'Es': -0.0751364293, 'Ud': 0.0762159678, 'Up': 0.0747995512, 'Us': 0.1226515157},
        'Ba': {'name': 'barium', 'atomic_number': 56, 'radii': 2.15, 'r_wave': 7.944, 'r_dens': 10.531, 'c_rep': 0.42199999,'mu_V': 0.04694725, 'mu_E': 0.11553187, 'loss': 0.36683712, 'r_quasinano': 7.7, 'sigma_quasinano': 12.0, 'configuration': '[Xe] 6s2 6p0 5d0', 'valence_shell':'6s2 6p0 5d0', 'Ed': -0.0734805983, 'Ep': -0.0440401985, 'Es': -0.1178125441, 'Ud': 0.1469536546, 'Up': 0.0963226003, 'Us': 0.160637835},
        'La': {'name': 'lanthanum', 'atomic_number': 57, 'radii': 2.07, 'r_wave': 5.56, 'r_dens': 10.239, 'c_rep': 0.39140389,'mu_V': 0.04322794, 'mu_E': 0.21454324, 'loss': 0.67994629, 'r_quasinano': 7.4, 'sigma_quasinano': 8.6, 'configuration': '[Xe] 5d1 6s2 6p0', 'valence_shell':'5d1 6s2 6p0', 'Ed': -0.113099977, 'Ep': -0.0484285961, 'Es': -0.1345153095, 'Ud': 0.2183047262, 'Up': 0.1031917406, 'Us': 0.1718991647},
        'Lu': {'name': 'lutetium', 'atomic_number': 71, 'radii': 1.87, 'r_wave': 7.798, 'r_dens': 13.486, 'c_rep': 0.37918957,'mu_V': 0.00071672, 'mu_E': 0.0308368, 'loss': 0.06459836, 'r_quasinano': 5.9, 'sigma_quasinano': 16.4, 'configuration': '[Xe] 4f14 5d1 6s2 6p0', 'valence_shell':'5d1 6s2 6p0', 'Ed': -0.0629263468, 'Ep': -0.0466482953, 'Es': -0.1696508517, 'Ud': 0.2199607118, 'Up': 0.1162748767, 'Us': 0.2050846573},
        'Hf': {'name': 'hafnium', 'atomic_number': 72, 'radii': 1.75, 'r_wave': 4.706, 'r_dens': 9.61, 'c_rep': 0.38058833,'mu_V': 0.01104093, 'mu_E': 0.05229058, 'loss': 0.15994929, 'r_quasinano': 5.2, 'sigma_quasinano': 14.8, 'configuration': '[Xe] 4f14 5d2 6s2 6p0', 'valence_shell':'5d2 6s2 6p0', 'Ed': -0.098531475, 'Ep': -0.050240709, 'Es': -0.18698778, 'Ud': 0.2442402441, 'Up': 0.1155660794, 'Us': 0.2160567903},
        'Ta': {'name': 'tantalum', 'atomic_number': 73, 'radii': 1.7, 'r_wave': 6.126, 'r_dens': 9.553, 'c_rep': 0.37766109,'mu_V': 0.01556161, 'mu_E': 0.10400607, 'loss': 0.25624479, 'r_quasinano': 4.8, 'sigma_quasinano': 13.8, 'configuration': '[Xe] 4f14 5d3 6s2 6p0', 'valence_shell':'5d3 6s2 6p0', 'Ed': -0.1319884769, 'Ep': -0.050560272, 'Es': -0.1994569598, 'Ud': 0.268278369, 'Up': 0.1132095769, 'Us': 0.2272629411},
        'W': {'name': 'tungsten', 'atomic_number': 74, 'radii': 1.62, 'r_wave': 4.696, 'r_dens': 9.107, 'c_rep': 0.37961591,'mu_V': 0.00960784, 'mu_E': 0.05851093, 'loss': 0.16154464, 'r_quasinano': 4.2, 'sigma_quasinano': 8.6, 'configuration': '[Xe] 4f14 5d4 6s2 6p0', 'valence_shell':'5d4 6s2 6p0', 'Ed': -0.1647739113, 'Ep': -0.0495352631, 'Es': -0.2093939901, 'Ud': 0.2903783045, 'Up': 0.1124558689, 'Us': 0.2378901498},
        'Re': {'name': 'rhenium', 'atomic_number': 75, 'radii': 1.51, 'r_wave': 5.871, 'r_dens': 8.097, 'c_rep': 0.40096803,'mu_V': 0.01888536, 'mu_E': 0.12448348, 'loss': 0.31197902, 'r_quasinano': 4.2, 'sigma_quasinano': 13.0, 'configuration': '[Xe] 4f14 5d5 6s2 6p0', 'valence_shell':'5d5 6s2 6p0', 'Ed': -0.1974381625, 'Ep': -0.0480225866, 'Es': -0.2178437132, 'Ud': 0.3101119395, 'Up': 0.1105418818, 'Us': 0.2478184354},
        'Os': {'name': 'osmium', 'atomic_number': 76, 'radii': 1.44, 'r_wave': 5.85, 'r_dens': 7.83, 'c_rep': 0.40676338,'mu_V': 0.00509122, 'mu_E': 0.01992229, 'loss': 0.05465289, 'r_quasinano': 4.0, 'sigma_quasinano': 8.0, 'configuration': '[Xe] 4f14 5d6 6s2 6p0', 'valence_shell':'5d6 6s2 6p0', 'Ed': -0.2301566396, 'Ep': -0.0462420614, 'Es': -0.2252906151, 'Ud': 0.3281348833, 'Up': 0.1080041383, 'Us': 0.2571302219},
        'Ir': {'name': 'iridium', 'atomic_number': 77, 'radii': 1.41, 'r_wave': 4.333, 'r_dens': 9.494, 'c_rep': 0.40373921,'mu_V': 0.00136019, 'mu_E': 0.02491016, 'loss': 0.056949, 'r_quasinano': 3.9, 'sigma_quasinano': 12.6, 'configuration': '[Xe] 4f14 5d7 6s2 6p0', 'valence_shell':'5d7 6s2 6p0', 'Ed': -0.2630227888, 'Ep': -0.0443081845, 'Es': -0.2320333007, 'Ud': 0.3449179703, 'Up': 0.1050765016, 'Us': 0.2659360336},
        'Pt': {'name': 'platinum', 'atomic_number': 78, 'radii': 1.36, 'r_wave': 3.322, 'r_dens': 13.209, 'c_rep': 0.41055319,'mu_V': 0.0052144, 'mu_E': 0.07495924, 'loss': 0.21978845, 'r_quasinano': 3.8, 'sigma_quasinano': 12.8, 'configuration': '[Xe] 4f14 5d9 6s1 6p0', 'valence_shell':'5d9 6s1 6p0', 'Ed': -0.2258445142, 'Ep': -0.0302200412, 'Es': -0.2072086319, 'Ud': 0.3405295638, 'Up': 0.0870143984, 'Us': 0.2463928419},
        'Au': {'name': 'gold', 'atomic_number': 79, 'radii': 1.36, 'r_wave': 3.362, 'r_dens': 9.801, 'c_rep': 0.39351539,'mu_V': 0.01377259, 'mu_E': 0.08150403, 'loss': 0.23940474, 'r_quasinano': 4.8, 'sigma_quasinano': 2.0, 'configuration': '[Xe] 4f14 5d10 6s1 6p0', 'valence_shell':'5d10 6s1 6p0', 'Ed': -0.2529405528, 'Ep': -0.0276298072, 'Es': -0.2109432793, 'Ud': 0.3974207159, 'Up': 0.0836248909, 'Us': 0.2539305754},
        'Hg': {'name': 'mercury', 'atomic_number': 80, 'radii': 1.32, 'r_wave': 4.193, 'r_dens': 12.259, 'c_rep': 0.1,'mu_V': 0.09877539, 'mu_E': 0.62906644, 'loss': 1.6117818, 'r_quasinano': 6.7, 'sigma_quasinano': 2.0, 'configuration': '[Xe] 4f14 5d10 6s2 6p0', 'valence_shell':'6s2 6p0', 'Ed': -0.0, 'Ep': -0.0381078929, 'Es': -0.2497360839, 'Ud': 0.0, 'Up': 0.0957925812, 'Us': 0.2901330468},
        'Tl': {'name': 'thallium', 'atomic_number': 81, 'radii': 1.45, 'r_wave': 5.462, 'r_dens': 19.181, 'c_rep': 0.43750009,'mu_V': 0.01755793, 'mu_E': 0.03277494, 'loss': 0.16526164, 'r_quasinano': 7.3, 'sigma_quasinano': 2.2, 'configuration': '[Xe] 4f14 5d10 6s2 6p1', 'valence_shell':'6s2 6p1', 'Ed': 0.0, 'Ep': -0.0867569215, 'Es': -0.3493538592, 'Ud': 0.0, 'Up': 0.1822716786, 'Us': 0.2952516265},
        'Pb': {'name': 'lead', 'atomic_number': 82, 'radii': 1.46, 'r_wave': 5.13158649, 'r_dens': 8.32929551, 'c_rep': 0.39015646,'mu_V': 0.0010387, 'mu_E': 0.03318099, 'loss': 0.07333151, 'r_quasinano': 5.7, 'sigma_quasinano': 3.0, 'configuration': '[Xe] 4f14 5d10 6s2 6p2', 'valence_shell':'6s2 6p2', 'Ed': 0.0, 'Ep': -0.128374504, 'Es': -0.4407907045, 'Ud': 0.0, 'Up': 0.2074563369, 'Us': 0.3070338583},
        'Bi': {'name': 'bismuth', 'atomic_number': 83, 'radii': 1.48, 'r_wave': 4.74, 'r_dens': 5.793, 'c_rep': 0.28666667,'mu_V': 0.01340951, 'mu_E': 0.03657246, 'loss': 0.1304243, 'r_quasinano': 5.8, 'sigma_quasinano': 2.6, 'configuration': '[Xe] 4f14 5d10 6s2 6p3', 'valence_shell':'6s2 6p3', 'Ed': 0.0, 'Ep': -0.1679279029, 'Es': -0.5300481715, 'Ud': 0.0, 'Up': 0.2294300843, 'Us': 0.3206488043},
        'Po': {'name': 'polonium', 'atomic_number': 84, 'radii': 1.4, 'r_wave': 4.418, 'r_dens': 4.04, 'c_rep': 0.30903125,'mu_V': 0.01302372, 'mu_E': 0.02510499, 'loss': 0.11416293, 'r_quasinano': 5.5, 'sigma_quasinano': 2.2, 'configuration': '[Xe] 4f14 5d10 6s2 6p4', 'valence_shell':'6s2 6p4', 'Ed': 0.0, 'Ep': -0.2066274508, 'Es': -0.6191847285, 'Ud': 0.0, 'Up': 0.2491136662, 'Us': 0.3347457186},
        'At': {'name': 'astatine', 'atomic_number': 85, 'radii': 1.5, 'r_wave': 4.545, 'r_dens': 4.791, 'c_rep': 0.38995204,'mu_V': 0.00264337, 'mu_E': 0.02143922, 'loss': 0.07082234, 'r_quasinano': 1.0, 'sigma_quasinano': 1.0, 'configuration': '[Xe] 4f14 5d10 6s2 6p5', 'valence_shell':'6s2 6p5', 'Ed': 0.0, 'Ep': -0.2450878144, 'Es': -0.7092139512, 'Ud': 0.0, 'Up': 0.2671645091, 'Us': 0.3489272298},
        'Rn': {'name': 'radon', 'atomic_number': 86, 'radii': 1.5, 'r_wave': 5.097, 'r_dens': 13.78, 'c_rep': 0.47493839,'mu_V': 7.57155855, 'mu_E': 0.01697728, 'loss': 16.21439002, 'r_quasinano': 1.0, 'sigma_quasinano': 1.0, 'configuration': '[Xe] 4f14 5d10 6s2 6p6 6d0', 'valence_shell':'6s2 6p6 6d0', 'Ed': 0.0362484909, 'Ep': -0.2836506462, 'Es': -0.8007475452, 'Ud': 0.0445084655, 'Up': 0.3027789669, 'Us': 0.3630284492},
        'Ra': {'name': 'radium', 'atomic_number': 88, 'radii': 2.21, 'r_wave': 5.744, 'r_dens': 20.258, 'c_rep': 0.1,'mu_V': 0.01727607, 'mu_E': 0.07891283, 'loss': 0.24233623, 'r_quasinano': 7.0, 'sigma_quasinano': 14.0, 'configuration': '[Rn] 7s2 7p0', 'valence_shell':'7s2 7p0', 'Ed': 0.0, 'Ep': -0.0352525177, 'Es': -0.1195923868, 'Ud': 0.0, 'Up': 0.089029896, 'Us': 0.162961998},
        'Th': {'name': 'thorium', 'atomic_number': 90, 'radii': 2.06, 'r_wave': 6.181, 'r_dens': 14.57, 'c_rep': 0.37304867,'mu_V': 0.00369051, 'mu_E': 0.02220853, 'loss': 0.0565253, 'r_quasinano': 6.2, 'sigma_quasinano': 4.4, 'configuration': '[Rn] 6d2 7s2 7p0', 'valence_shell':'6d2 7s2 7p0', 'Ed': -0.113371953, 'Ep': -0.0448638928, 'Es': -0.161174602, 'Ud': 0.2098934916, 'Up': 0.1023998075, 'Us': 0.185481095},}

        self.path_dict = {'dimer':  {'path':'MRGXMGR','npoints':101},
                'fcc':    {'path':'WXGLKGL','npoints':101},
                'bcc':    {'path':'PGHPGN','npoints':101}, #PGHPGN
                'beta-Sn':{'path':'GXMGZPNZ1XP','npoints':101},
                'diamond':{'path':'WXGLKGL','npoints':101},
                'AA-graphite':{'path':'GKHAGMLA','npoints':101},
                'wz_ZnO':{'path':'GMKGALHA,LM,KH','npoints':101},
                'CuZn': {'path':'GXMGRX,MR', 'npoints': 101},
                'CuZnO3': {'path': 'GXMGRX,MR', 'npoints': 101},
                'ZnAg2O4':{'path': 'GXMGZRAZ,XR,MA', 'npoints': 101},
                'ZnCu2O4':{'path': 'GXMGZRAZ,XR,MA', 'npoints': 101},
}

    def set_variables(self, 
                      confinement_r0: dict,
                      confinement_sigma: dict,
                      xc: str,
                      superposition: str,
                      workdir: str='./',
                      slator_p: float='./',
    ):
        self.confinement_r0 = confinement_r0
        self.confinement_sigma = confinement_sigma
        self.symbols = list(confinement_r0.keys())
        self.workdir = workdir
        self.slator_p = slator_p
        self.xc = xc
        self.superposition = superposition

        

    def get_orbit(self, valence_shell:str) -> tuple:
            """
            Parser valence_shell string occupation, valances, orbits

            Parameters:
            -----------
                valence_shell(str): valence shell of an element
                eg. '4d8 5s1 5p0' for 'Rh' 

            Return:
                eg. (occupations,valences,orbits,valence_electron_numbers) = ({'4d': 8, '5s': 1, '5p': 0}, ['4d', '5s', '5p'],['d', 's', 'p'], 9)
            """

            info, occupations, valences, orbits = [], {}, [], []


            [info.append(i) for i in re.split('\s+', valence_shell.strip())]

            valence_electron_numbers = 0  # number of valence electrons

            for orb in info:
                # maches numbers from the end
                num_elec = re.findall(r'\d+$', orb)
                occupations[orb[:2]] = int(num_elec[0])
                valence_electron_numbers += int(num_elec[0])
                # append valences to [valences] and
                orbits.append(orb[1])
                valences.append(orb[0:2])      # [orbits]
            return(occupations, valences, orbits, valence_electron_numbers)


    def get_bandpath_for_dftb(self, atoms:Atoms, kpts:int) -> Dict:
        """This function sets up the band path according to Setyawan-Curtarolo conventions.

        Parameters:
        -----------
        atoms(Atoms): ase.Atoms object
            The molecule or crystal structure.
        kpts(int): The number of k-points among two special kpoint positions.

        Returns(Dict):
        -------------

        """
        from ase.dft.kpoints import kpoint_convert, parse_path_string

        # atoms.pbc = pbc
        # path = parse_path_string(
        #     atoms.cell.get_bravais_lattice(pbc=atoms.pbc).bandpath().path
        # )

        path = parse_path_string(kpts['path'])
        # lists of path segments
        points = atoms.cell.get_bravais_lattice(pbc=atoms.pbc).bandpath().special_points
        segments = []
        for seg in path:
            section = [(i, j) for i, j in zip(seg[:-1], seg[1:])]
            segments.append(section)


        output_bands = []
        output_bands = np.empty(shape=(0, 3))
        index = kpts['npoints']
        for seg in segments:
            # output_bands.append("## Brillouin Zone section Nr. {:d}\n".format(index))
            for num, sec in enumerate(seg):
                dist = np.array(points[sec[1]]) - np.array(points[sec[0]])
                npoints = index
                if num == 0:
                    dist_matrix = np.linspace(points[sec[0]], points[sec[1]], npoints)
                else:
                    dist_matrix = np.linspace(points[sec[0]], points[sec[1]], npoints)[1:,:]
                output_bands = np.vstack((output_bands, dist_matrix))

        return {'path':path, 'kpts':output_bands}
    

    def get_bolt_factor(self, atoms:Atoms, option:str, temperature:float) -> tuple:
        '''
        Calculate the Boltzmann factor as the weight of each molecule
        Parameters:
        ----------
            atoms(Atoms): a list of Atoms object

            option(str): whether ouput boltzmann factor weight or uniformly equal to 1/num_atoms or 1 for all
                         eg. "boltzmann" or "uniform" and "identical"

            temperature(float): temperature in K
        
        Returns:
            bf_weight_norm(List): list of boltzmann factor weight for each structure
            bf_weight_norm11(List): list of Boltzmann factors weight for each structure's EOS expansion

        '''
        T = temperature
        atoms_number = len(atoms)
        if option == "boltzmann":
            energy_per_atom = [dft_mole.get_total_energy() / dft_mole.get_global_number_of_atoms() for dft_mole in atoms]
            energy_per_atom_min = np.array(energy_per_atom).min()

            """Bf = exp(-(E-Emin)/(kB*T)) """
            bf_weights = [np.exp(-(energy_per_atom[i] - energy_per_atom_min)/(kB * T)) for i in range(atoms_number)]

            """Bf_norm = Bf/sum(Bf)"""
            bf_weight_norm = bf_weights/sum(bf_weights)

            # When fitting the EOS, the number of data points is 11 times the number of atoms
            bf_weight_norm11 = np.array([[bf_weight_norm[n]] * 11 for n in range(atoms_number)]).flatten()

            """Bf = 1/num_of_structures"""
        elif option == "uniform":
            bf_weight_norm = np.array([1/atoms_number] * atoms_number)
            bf_weight_norm11 = np.array([1/atoms_number] * atoms_number * 11)
        elif option == "identical":
            bf_weight_norm = np.array([1] * atoms_number)
            bf_weight_norm11 = np.array([1] * atoms_number * 11)
        else:
            raise ValueError('option should be "boltzmann", "uniform" or "identical"')
        return(bf_weight_norm, bf_weight_norm11)
    
    
    def dist_min(self, atoms:Atoms) -> list:
        """
        Get the minimum atomic distance in a bulk structure

        Parameters:
        -----------
        atoms(Atoms): list of Atoms object
        
        Return:
            d_min(list): the minimum atomic distance in a bulk
        """
        
        d_min = []
        # file = open('dist_min.txt', 'w')

        for num, mole in enumerate(atoms):

            if mole.get_global_number_of_atoms() > 1: 
                dist = mole.get_all_distances(mic=True).flatten()
                dist = dist[dist!=0.]
                min_dist = np.min(dist)
                d_min.append(min_dist)
                # file.write(str(num) + ' ' + str(min_dist) + '\n')

            #For one atom bulk, the minimum distance is the minimum value of abc
            else:
                abc = mole.cell.cellpar()[:3]
                min_dist = np.min(abc)
                d_min.append(min_dist)
                # file.write(str(num) + ' ' + str(min_dist) + '\n')
        # file.close()
        return(d_min)


    # def hot_hamiltonian(self, const, sig, xc, element) -> dict:
    def hot_hamiltonian(self, r0_x:list, sigma_x:list, xc:str, symbol_x:str, label:str='ptbp'): 
        """
        Atomic DFT calculation for isolated atom

        Parameter:
        ----------
            r0_x(list): r0 parameters for confinement potential, [wavefunction r0, density function r0]
                e.g. [1,2]

            sigma_x(list): sigma value for confinement potential
                e.g. 2.0

            xc(str): exchange correlation method
                e.g. 'GGA_X_PBE+GGA_C_PBE'

            symbol_x(str): element symbol
                e.g. 'H'

        Returns:
        --------
            Return AtomicDFT, 
        """

        # Get the valence shell information store in self.atoms_info
        if label.lower() == 'ptbp':
            occupations, valences, orbits, outer_ele = self.get_orbit(
                self.atoms_info[symbol_x]['valence_shell'])
            
            config = self.atoms_info[symbol_x]['configuration']
            print("Occupations:", occupations, "\nValences:",
                valences, "\nOrbits:", orbits)


            # Get previous calculated eigenvalues and hubbard values store in self.atoms_info
            atom_info = {'hubbardvalues':{}, 'eigenvalues':{}}
            Ed, Ep, Es = self.atoms_info[symbol_x]['Ed'], self.atoms_info[symbol_x]['Ep'], self.atoms_info[symbol_x]['Es']
            Ud, Up, Us = self.atoms_info[symbol_x]['Ud'], self.atoms_info[symbol_x]['Up'], self.atoms_info[symbol_x]['Us']
        elif label.lower() == 'quasinano':
            occupations, valences, orbits, outer_ele = self.get_orbit(
                self.atoms_info_quasinano[symbol_x]['valence_shell'])
            
            config = self.atoms_info_quasinano[symbol_x]['configuration']
            print("Occupations:", occupations, "\nValences:",
                valences, "\nOrbits:", orbits)
            

            # Get previous calculated eigenvalues and hubbard values store in self.atoms_info
            atom_info = {'hubbardvalues':{}, 'eigenvalues':{}}
            Ed, Ep, Es = self.atoms_info_quasinano[symbol_x]['Ed'], self.atoms_info_quasinano[symbol_x]['Ep'], self.atoms_info_quasinano[symbol_x]['Es']
            Ud, Up, Us = self.atoms_info_quasinano[symbol_x]['Ud'], self.atoms_info_quasinano[symbol_x]['Up'], self.atoms_info_quasinano[symbol_x]['Us']
        else:
            raise ValueError('label should be "ptbp" or "quasinano"')
        

        for i in range(len(orbits)):
            if orbits[i] == 'd':
                atom_info['hubbardvalues'][orbits[i]] = Ud
                atom_info['eigenvalues'][valences[i]] = Ed
            elif orbits[i] == 'p':
                atom_info['hubbardvalues'][orbits[i]] = Up
                atom_info['eigenvalues'][valences[i]] = Ep
            elif orbits[i] == 's':
                atom_info['hubbardvalues'][orbits[i]] = Us
                atom_info['eigenvalues'][valences[i]] = Es
        print('info:', atom_info)

        atom_info['occupations'] = occupations



        # Set wavefunction confinement parameter for each valence        
        conf_wf = {}
        for val in valences:  # set confinement for each valence
            conf_wf[val] = PowerConfinement(r0=(r0_x[0]), s=sigma_x)

        # Set density confinement parameter
        conf_dens = PowerConfinement(r0=r0_x[1], s=sigma_x)


        # Apply confinement potential and solving the pseudo-atom orbitals
        atom = AtomicDFT(symbol_x,
                         xc=xc,
                         configuration=config,
                         valence=valences,
                         scalarrel=False,
                         confinement=conf_dens,
                         wf_confinement=conf_wf,
                         )
        atom.run('potential')

        atom.info = {'hubbardvalues': {}}
        atom.info['hubbardvalues'] = atom_info['hubbardvalues']
        atom.info['occupations'] = atom_info['occupations']
        atom.info['eigenvalues'] = atom_info['eigenvalues']

        return atom, config, r0_x
    

    # def generate_skf(self, symbol_list, r0_dict, sigma_list, xc, superposition):
    def generate_skf(self, symbol_pair:list, r0_pair:dict, sigma_pair:dict, xc:str, superposition:str, label:str='ptbp'):
    

        """
        To generate skf file for one symbol pair [symbol_a, symbol_b]
        
        Parameters:
        ----------
            Theory:
                V_conf = (r_0/r_cov)^{\sigma}
                where, r_cov = covalent_radii[atomic_numbers[element]] / Bohr


            symbol_pair(list): element symbol list 
                eg. ['H', 'C']

            r0_pair(dict): r0 parameters for confinement potential, eg. {'H': [wavefunction r0, density function r0], 'C': [wavefunction r0, density function r0]}
                eg. {'H': [1,2], 'C': [2,3]}

            sigma_pair(dict): sigma value for confinement potential
                eg. {'H': 2.0, 'C': 2.0}

            xc: exchange correlation method 
                eg. 'GGA_X_PBE+GGA_C_PBE' 

            superposition: superposition method
                eg. 'denisty' or 'potential'

        Return:
        -------
            skf file
        """


        # Build subfolder to avoid confilication of multiprocessing 
        name_subfolder = 'SKF_' + '_'.join(symbol_pair)
        if not os.path.exists(name_subfolder):
            os.mkdir(name_subfolder)
            os.chdir(name_subfolder)
        else:
            os.chdir(name_subfolder)


        # Calculate hamiltonian for each symbol(element)
        # To get pseudo-atom orbitals
        hamitonian_dict = {}  # store hamiltonian for each symbol
        hot_log = open('../pot.out', 'a') 
        for i, symbol_x in enumerate(symbol_pair):
            hot_log.write("calculate {}'s hamitonian\n".format(symbol_x))
            hot_log.flush()
            r0_x = r0_pair[symbol_x]
            sigma_x = sigma_pair[symbol_x]

            # Call hot_hamiltonian function to calculate hamiltonian for each symbol(element)
            atom_x, config_x, r0_x = self.hot_hamiltonian(r0_x, sigma_x, xc, symbol_x, label)
            hamitonian_dict[symbol_x] = [atom_x, config_x, r0_x]



        # Write skf file
        hot_log.write("###Write skf file###\n")
        for i, symbol_a in enumerate(symbol_pair):
            for k, symbol_b in enumerate(symbol_pair):
                hot_na = symbol_a + '-' + symbol_b + '.skf'
                print("writting {} file\n".format(hot_na))
                hot_log.flush()

                atom_a, config_a, r0_a = hamitonian_dict[symbol_a][0], hamitonian_dict[symbol_a][1], hamitonian_dict[symbol_a][2]
                atom_b, config_b, r0_b = hamitonian_dict[symbol_b][0], hamitonian_dict[symbol_b][1], hamitonian_dict[symbol_b][2]


                #==============================================================#
                # The code will not overwrite old skf file, but use it instead #
                # So, please remove old SKF file manually                      #
                #==============================================================#
                if os.path.exists(hot_na): #and os.path.getsize(hot_na):
                    print(f"There is existed old skf file: {hot_na}, I am gonna remove it")
                    os.system(f"rm {hot_na}")

                rmin, dr, N = 0.4, 0.02, 600

                sk = SlaterKosterTable(atom_a, atom_b, txt='-')
                sk.run(rmin, dr, N, superposition=superposition, xc=xc)

                if atom_a == atom_b:
                    sk.write(symbol_a + '-' + symbol_b + '.skf',
                                eigenvalues=atom_a.info['eigenvalues'],
                                hubbardvalues=atom_a.info['hubbardvalues'],
                                occupations=atom_a.info['occupations'], spe=0.)
                    # For hotcent 2.0, I need to rename the skf file from "symbol-symbol_offsite2c.skf" to "symbol-sybmol.skf"
                    hotcent_name = symbol_a + '-' + symbol_b + '_offsite2c.skf'
                    os.rename(hotcent_name, f"{symbol_a}-{symbol_b}.skf")
                else:
                    # For hotcent 2.0, I need to rename the skf file from "symbol-symbol_offsite2c.skf" to "symbol-sybmol.skf"
                    hotcent_name = symbol_a + '-' + symbol_b + '_offsite2c.skf'
                    sk.write(symbol_a + '-' + symbol_b + '.skf')
                    os.rename(hotcent_name, f"{symbol_a}-{symbol_b}".skf)
                
                hot_log.write('config_a=' + str(config_a) + '\n')
                hot_log.write('config_b=' + str(config_b) + '\n')
                hot_log.write('#r0 for eigenvalue calculation:a,b=' + str(r0_a) + ',' + str(r0_b) + '\n')
                hot_log.flush()


        os.system("mv *.skf ../")
        os.chdir('..')

        hot_log.close()
        
    def dist_coor(self, coor):

        dist = 0
        for xyz in coor[:]:
            x, y, z = xyz
            dist += math.sqrt(x**2 + y**2 + z**2)
        return dist



    def hotpot(self, xc:str, superposition:str):
        # """
        # Generate skf file

        # Parameters:
        # ----------
        #     xc: exchange correlation method
        #         e.g. 'GGA_X_PBE+GGA_C_PBE'
        #     superposition: superposition method
        #         e.g. 'denisty' or 'potential'

        # Return:
        # -------
        #     skf file
        # """
        # #eg. constant = np.linspace(5.5,10,10);elements = ['H', 'C']
        # r0_dict = self.r0_dict
        # sigma_list = self.sigma_list
        # symbols = self.symbols
        # rmin, dr, N = 0.4, 0.02, 600  # the distance range for skf file

        # for i, symbol_a in enumerate(symbols):
        #     for k, symbol_b in enumerate(symbols):




        cons = self.cons
        elements = self.elements  # ['H']
        sigma = self.sigma
        hot_log = open('pot.out', 'w')
        rmin, dr, N = 0.4, 0.02, 600

        hot_log.write('#---------------rconf=' + str(cons) + '---------------\n')
    #    log.write(cal_dir + '\n')
        for i, ela in enumerate(elements):
            for k, elb in enumerate(elements):
                hot_log.write('writting ' + ela + '_' + elb + '.skf\n')
                hot_log.flush()
                cons_a = cons[ela]
                cons_b = cons[elb]
                sigma_a = sigma[ela]
                sigma_b = sigma[elb]
                hot_log.write('{}\t{}\n'.format(cons_a, cons_b))
                hot_na = ela + '-' + elb + '.skf'
                # if hot_na exists and not blank
                if os.path.exists(hot_na) and os.path.getsize(hot_na):
                    print("Hi, {} existed! I am gonna backup it to {}".format(hot_na, hot_na.lower()))
                    os.rename(hot_na, hot_na.lower())
                    # skf_f = open(hot_na, 'w')
                    # for l in open(hot_na.lower(), 'r').readlines():
                    #     if l != 'Spline \n':
                    #         skf_f.write(l)
                    #     else:
                    #         break
                    # skf_f.close()
                else:
                    print("Hi, {} not existed! I am gonna create it".format(hot_na))
                    
                if cons_a == cons_b and ela == elb:
                    print("Hi, {} and {} are the same! I just need to caculate it once".format(ela, elb))
                    atom_a, config_a, r0_a = self.hot_hamiltonian(
                        cons_a, sigma_a, xc, ela)
                    atom_b, config_b, r0_b = atom_a, config_a, r0_a
                else:
                    print("Hi, {} and {} are different! I need to caculate both".format(ela, elb))
                    atom_a, config_a, r0_a = self.hot_hamiltonian(
                        cons_a, sigma_a, xc, ela)
                    atom_b, config_b, r0_b = self.hot_hamiltonian(
                        cons_b, sigma_b, xc, elb)

                # run slaterkostertable
                sk = SlaterKosterTable(atom_a, atom_b, txt='-')
                sk.run(rmin, dr, N, superposition=superposition, xc=xc)

                # when ela == elb (eg. H-H or C-C),
                # writting hubbardvalues, eigenvalues, occupations to skf file.
                if ela == elb:
                    sk.write(ela + '-' + elb + '.skf',
                                eigenvalues=atom_a.info['eigenvalues'],
                                hubbardvalues=atom_a.info['hubbardvalues'],
                                occupations=atom_a.info['occupations'], spe=0.)
                # when ela \= elb, don't need write this information to skf file.
                else:
                    sk.write(ela + '-' + elb + '.skf')
                hot_log.write('config_a=' + str(config_a) + '\n')
                hot_log.write('config_b=' + str(config_b) + '\n')
                hot_log.write('#r0 for eigenvalue calculation:a,b=' +
                    str(r0_a) + ',' + str(r0_b) + '\n')
        hot_log.close()

    def error_callback(self, exception):
        print('Exception:', exception)

    def full_hotcent(self, superposition:str, opt_elem:list=[], label:str='ptbp'):
        """
        Generate full SKF files (band+repulsion) based on existed parameters

        Parameters:
            xc(str): exchange correlation method 
                eg. 'GGA_X_PBE+GGA_C_PBE'

            superposition(str): superposition method 
                eg. 'denisty' or 'potential'

            opt_symbol: list of symbol that need to optimize confinement parameters
                eg. ['O']

        """
        xc = self.xc
        opt_symbol = [chemical_symbols[i] for i in opt_elem]
        r0 = self.confinement_r0
        symbols = self.symbols
        sigma = self.confinement_sigma

        hot_log = open('pot.out', 'w') 

        # if opt_symbol != None:
        if len(opt_symbol) == 0:
            if len(symbols) == 1:
                symbol_pairs = [(symbols[0], symbols[0])]
            else:
                symbol_skf = symbols
                symbol_pairs = [comb for comb in combinations(symbol_skf, 2)]
        elif len(opt_symbol) >= 1:
            symbol_skf = [i for i in symbols if i not in opt_symbol] 

            symbol_pairs = []
            [symbol_pairs.append([elx] + opt_symbol) for elx in symbol_skf]

        # else:
        #     symbol_pairs = [symbols[0], symbols[0]]

        hot_log.write('#---------------rconf=' + str(r0) + '---------------\n')
        hot_log.close()

        pool = mp.Pool(processes=len(symbol_pairs)) if len(symbol_pairs) != 0 else mp.Pool(processes=1)

        num_cores = int(mp.cpu_count())
        print("CPU cores: " + str(num_cores))
        print("Number of jobs: " + str(len(symbol_pairs)))


        for i, pair in enumerate(symbol_pairs):
            symbol_pair = list(pair)

            r0_pair = {sym: r0[sym] for sym in symbol_pair}
            sigma_pair = {sym: sigma[sym] for sym in symbol_pair}

            pool.apply_async(self.generate_skf, args=(symbol_pair, r0_pair, sigma_pair, xc, superposition, label), error_callback=self.error_callback)
        
        pool.close()
        pool.join()
        with open('pot.out', 'a') as hot_log:
            hot_log.write("Parallel hotcent calculation finished!\n")
        
    def monkhorstpack2kptdensity(self, atoms, k_density):

        """Convert Monkhorst-Pack grid to k-point density.
        atoms (ase.atoms.Atoms): Atoms object.
        k_grid (list): [nx, ny, nz].
        Returns:
            float: Smallest line-density.
        """

        # assert len(k_grid) == 3, "Size of k_grid is not 3."

        recipcell = atoms.cell.reciprocal()
        kd = [] # k-point density
        ks = [] # k-point space
        # initial a array for k_grids, with 3 columns
        # k_grids = np.empty((1,3))
        k_grid_abc = []
        for i in range(3):
            if atoms.pbc[i]:
                k_grida = int(k_density * (2 * pi * sqrt((recipcell[i] ** 2).sum())))
                if k_grida == 0:
                    k_grida = 1
                k_grid_abc.append(k_grida) # current k_grid
                # k_grids = np.vstack((k_grids, k_grid_abc))
                # kptdensity = k_grid[i] / (2 * pi * sqrt((recipcell[i] ** 2).sum()))
                kptspace =  sqrt((recipcell[i] ** 2).sum()) / k_grida
                ks.append(kptspace)
        return ks, k_grid_abc
    

    def run_calculator(self, dft_data:Atoms, poly_rep:str, SCC:str, kpts_dens, bolt_f):
        """
        Calculate DFTB+ and delta forces between DFT and DFTB

        Parameters:
            dft_data: dft Atoms object
            opt: 'Yes' or 'No'. 'yes', when skf without repulsive pot.  
            kpts: list eg. kpts = [11,11,11]
            bolt_f: Boltzmann factor 1x6 array

        Return:
            Atoms object
        """
        dftb_args = {"calc_type": "static",
                     "poly_rep" : poly_rep,
                     "SCC"      : SCC,
                     "slater_p" : self.slator_p,
        }

        atoms = []
        for num, mole_dft in enumerate(dft_data):
            atoms_new = Atoms(mole_dft.get_chemical_symbols(), positions=mole_dft.get_positions())
            atoms_new.set_cell(mole_dft.get_cell())
            atoms_new.set_pbc(mole_dft.get_pbc())


            formula = mole_dft.get_chemical_formula()
            force_dft = mole_dft.get_forces()

            # calculate dftb forces
            if bolt_f[num] < 0.1:
                kpts_dftb = [1,1,1]
            else:
                ks, kpts_dftb = self.monkhorstpack2kptdensity(mole_dft, kpts_dens)

            dftb_args['kpts'] = kpts_dftb
            calc = self.dftbplus_calculator(atoms_new, dftb_args)
            # 1st 'No' means get repulsion from splines
            # 2nd 'No' means no SCC
            # calc = self.calc_dftbplus(mole_dftb, formula, opt, 'No', self.slator_p, kpts)
            atoms_new.calc = calc
            # energy     = atoms_new.get_potential_energy()
            force_dftb = atoms_new.get_forces()

            # delta_forces_dftb = force_dft - force_dftb
            # mole_dftb.set_array('delta_forces_dftb', delta_forces_dftb)
            atoms.append(atoms_new)
            # ase.io.write('delta_forces.xyz', atoms)
        return atoms

    def delta_forces(self, dft_data, opt, scc, kpts, bolt_f66):
        """
        Calculate DFTB+ and delta forces between DFT and DFTB

        Parameters:
            dft_data: dft Atoms object
            opt: 'Yes' or 'No'. 'yes', when skf without repulsive pot.  
            kpts: list eg. kpts = [11,11,11]
            bolt_f: Boltzmann factor 1x6 array

        Return:
            Atoms object
        """
        molecules = []
        for num, mole_dftb in enumerate(dft_data):
            formula = mole_dftb.get_chemical_formula()
            force_dft = mole_dftb.get_forces()

            # calculate dftb forces
            if bolt_f66[num] < 0.1:
                kpts_dftb = [1,1,1]
            else:
                kpts_dftb = kpts
            calc = self.dftbplus_calculator('static', mole_dftb, formula, opt, scc, self.slator_p, kpts_dftb)
            # 1st 'No' means get repulsion from splines
            # 2nd 'No' means no SCC
            # calc = self.calc_dftbplus(mole_dftb, formula, opt, 'No', self.slator_p, kpts)
            mole_dftb.calc = calc
            force_dftb = mole_dftb.get_forces()

            delta_forces_dftb = force_dft - force_dftb
            mole_dftb.set_array('delta_forces_dftb', delta_forces_dftb)
            molecules.append(mole_dftb)
            ase.io.write('delta_forces.xyz', molecules)
        return molecules


    # def dftbplus_calculator(self, calc_type, mole, formula, poly_rep, SCC, slater_p, kpts):
    def dftbplus_calculator(self, mole:Atoms, dftb_args:dict):
        '''
        mole is atoms object
        formula is the str name of molecules
        Parameters:
            type: 'band' or 'static' or 'opt_geometry'
            opt: Yes when slater file without repulsive pot
            formula: whatever string lable
            h_scc = No when don't using SCC calculation
            slater_p is the directory of slator-koster file
            kpts: (20, 20, 20)
        '''
        # poly_rep = 'SetForAll {' + str(opt) + '}'
        calc_type, poly_rep, SCC, kpts, slater_p = dftb_args['calc_type'], dftb_args['poly_rep'], dftb_args['SCC'], dftb_args['kpts'], dftb_args['slater_p']
        formula = mole.get_chemical_formula()
        
        if calc_type == 'static':
            calc = Dftb(atoms=mole, label=str(formula), kpts=kpts,

                        Hamiltonian_='DFTB',
                        Hamiltonian_SCC=SCC,
                        Hamiltonian_SCCTolerance=1e-5,
                        Hamiltonian_MaxSCCIterations=1000,
                        Hamiltonian_ConvergentSCCOnly = 'No',
                        Hamiltonian_ReadInitialCharges='No', # Static calculation
                        Hamiltonian_ForceEvaluation='dynamics',


                        # ONLY FOR SCC CONVERGENCE
                        Hamiltonian_Fermi_='', Hamiltonian_Fermi_Temperature=2000,
                        Hamiltonian_MethfesselPaxton_='', Hamiltonian_MethfesselPaxton_Order=2,
                        Parallel_='', Parallel_Groups=1,


                        # SlaterKosterFiles
                        Hamiltonian_SlaterKosterFiles_='Type2FileNames',
                        Hamiltonian_SlaterKosterFiles_Prefix=slater_p,
                        Hamiltonian_SlaterKosterFiles_Separator='"-"',
                        Hamiltonian_SlaterKosterFiles_Suffix='".skf"',
                        Hamiltonian_MaxAngularMomentum_='',


                        # Repulsive Potential
                        Hamiltonian_PolynomialRepulsive=f'SetForAll {{{poly_rep}}}',


                        # Options
                        Options_='', Options_WriteResultsTag='Yes',        # Default:No
                        Options_WriteDetailedOut='Yes', # Default:
                        )


        elif calc_type == 'band':
                    calc = Dftb(atoms=mole, label=str(formula), kpts=kpts,

                    Hamiltonian_='DFTB',
                    Hamiltonian_SCC=SCC, Hamiltonian_SCCTolerance=1e-5,
                    Hamiltonian_MaxSCCIterations=1,
                    Hamiltonian_ReadInitialCharges='Yes',
                    Hamiltonian_SlaterKosterFiles_Prefix=slater_p,
                    Hamiltonian_SlaterKosterFiles_Separator='"-"',
                    Hamiltonian_SlaterKosterFiles_Suffix='".skf"',
                    Hamiltonian_PolynomialRepulsive=f'SetForAll {{{poly_rep}}}',
                    Hamiltonian_ForceEvaluation='dynamics',

                    Options_='', Options_WriteResultsTag='Yes',        # Default:No
                    Options_WriteDetailedOut='Yes',
                    )
        elif calc_type == 'xtb':
            calc = Dftb(atoms=mole, label=str(formula),
                    kpts=kpts,
                    Hamiltonian_='xTB',
                    Hamiltonian_Method="GFN1-xTB",
                    Hamiltonian_SCC=SCC,
                    Hamiltonian_SCCTolerance=1e-5,
                    Hamiltonian_MaxSCCIterations=1000,
                    Hamiltonian_ConvergentSCCOnly = 'No',
                    Hamiltonian_ReadInitialCharges='No', # Static calculation
                
                    
                    #Hamiltonian_Filling_='',
                    #Hamiltonian_Filling_MethfesselPaxton_='', 
                    #Hamiltonian_Filling_MethfesselPaxton_Order=2,
                    #Hamiltonian_Filling_MethfesselPaxton_Temperature=2000,
                    Hamiltonian_SlaterKosterFiles_='',
                    Hamiltonian_MaxAngularMomentum_='',


                    Options_='', Options_WriteResultsTag='Yes',        # Default:No
                    Options_WriteDetailedOut='Yes', # Default:
                    Parallel_='', Parallel_Groups=64,
        )
        else:
            raise ValueError('type must be band or static')
        return calc

    def calc_band_binary(self, conf_parameters, dft_atoms, kpt_dens, bolt_f):
        """
        Band calculation for DFTB+

        Parameters
        ----------
        para_list : list
            List of parameters.

        """
        
        # Label
        para_name = '_'.join([str(round(i,3)) for i in conf_parameters])


        # Calculate the band structure for each structure
        for num, mole in enumerate(dft_atoms):

            # # config = mole.info['structure_name']
            # print("\n#===========Electronic calculation for {}============#".format(config))
            print(f"\n#===========Electronic calculation for {num}/{len(dft_atoms)}============#")
            print("bolt_f:", bolt_f[num])
            try:
                configuration = mole.info['structure_name']
            except:
                configuration = 'band_' + str(num)
            print('Band structure:', configuration)

            os.system('rm charges.bin')

            if bolt_f[num] < 0.1:
                continue
            else:
        # ============================================================================ #
        #       # Static calculation
                # Name the output file also for getting kpts path
                # The input file need to have a info named "structure_name"
                # To (1) label output file (2) get the kpts path for path_dict


                ks, kpts = self.monkhorstpack2kptdensity(mole, kpt_dens)

                dftb_args = {"calc_type": "static",
                             "poly_rep" : 'Yes',
                             "SCC"      : 'Yes',
                             "slater_p" : self.slator_p,
                             "kpts"     : kpts,
                }

                mole_static = copy.deepcopy(mole)

                mole_static.calc = self.dftbplus_calculator(mole_static, dftb_args)

                # Attention: 
                # (1) First Yes means read repulsion from polynomial, however, it is 0 in skfiles, shortly, means no repulsion
                # (2) Second Yes means open "SCC" calculation
                # mole.calc = self.dftbplus_calculator('static', mole, 'res', 'Yes', 'Yes', './', kpts)

                print('------------\nStep1: Static calculation\nEnergy:', mole_static.get_potential_energy())
                # mole.get_forces()
                
                fermi_static = mole_static.calc.get_fermi_level()
                print('Static Fermi_level:', fermi_static)

            # ============================================================================ #
            #   # Band structure calculation

                # Two ways to get kpts path
                # (1) Use the known path_dict
                # (2) Use the default path from ase.dft.kpoints.bandpath
                try:
                    kpts_spe = self.path_dict[configuration]
                    kpts_path=self.get_bandpath_for_dftb(mole, kpts=kpts_spe)
                except Exception as e:
                    print('Find kpts path Error:')
                    path_dict = mole.cell.get_bravais_lattice().get_special_points()
                    kpts_path = ase.dft.kpoints.bandpath(path_dict, mole.cell)

                mole_band = copy.deepcopy(mole_static)
                # mole_band.calc = self.dftbplus_calculator('band', mole_band, 'res', 'Yes', 'No', './',kpts_path)
                dftb_args['kpts'] = kpts_path
                mole_band.calc = self.dftbplus_calculator(mole_band, dftb_args)
                print('------------\nStep2: Band structure calculation, \nEnergy:', mole_band.get_potential_energy())

                bs = mole_band.calc.band_structure()
                fermi_band = mole_band.calc.get_fermi_level()
                print("Band Fermi_level:", fermi_band, '\n')

            # ============================================================================ #
            #   # Attention to compare the fermi level between static and band
                # The density of kpoints could casually cause the difference of fermi level
                # Here, we use fermi level from static calculation!
                if abs(fermi_band - fermi_static)/abs(fermi_static) > 0.1:
                    print('Warning!!! The Fermi_level between static and band is not consistent more than 10%!!!')

            # ========================================================================== #
                # Correct the Fermi level to HVB(highest valence band) = 0
                # PS: for semiconductor or insulator, fermi level from DFTB+ tends to located in the center of band gap
                # So, It is necessary to shift.

                # Get band gap from ASE bandgap as reference
                from ase.dft import bandgap
                band_gap = round(bandgap.bandgap(mole_band.calc)[0],5)  # Band gap from DFTB+
                print('Band gap from bandgap.bandgap(mole.calc):', band_gap, '\n')

                # When semiconductor or insulator, one need to find the HVB and LCB (lowest conduction band)
                if band_gap != 0.0: 

                    # Collect the valence shell information 
                    dftb_tot_chemical_symbols = mole.get_chemical_symbols() #eg:['Zn', 'Zn', 'O', 'O']
                    dftb_num_valence_electron = np.sum(self.get_orbit(self.atoms_info[syms]['valence_shell'])[3] for syms in dftb_tot_chemical_symbols)

                    # Get the band for HVB and LCB
                    dftb_json_energies = bs.energies
                    dftb_LCB_e = dftb_json_energies[0, :, ceil(dftb_num_valence_electron/2)].min()
                    dftb_HVB_e = dftb_json_energies[0, :, ceil(dftb_num_valence_electron/2) - 1].max()
                # Assure the band gap is consistent with ASE bandgap
                    band_from_json = round(dftb_LCB_e - dftb_HVB_e,3) if dftb_LCB_e - dftb_HVB_e > 0 else 0.0
                    print("LCB_e={:.4f}\nHVB_e={:.4f}\nDFTB_gap:{:.5f}".format(dftb_LCB_e, dftb_HVB_e, band_from_json))
                    assert round(band_gap, 3) == round(band_from_json, 3), "The band gap of bandgap.bandgap() is not consistent with json file!"

                # Correction information
                    print("Here I correct the fermi level from {} to HVB({})".format(fermi_band, dftb_HVB_e))


                    # Save both orign and corrected one
                    origin_band = copy.deepcopy(bs)
                    origin_band._reference = fermi_static
                    origin_band.write(configuration + '_' + para_name + '_o.json') # save the original band structure just for reference

                    bs._reference = dftb_HVB_e
                    bs.write(configuration + '_' + para_name + '.json')

                else:
                    bs.write(configuration + '_' + para_name + '.json')

        print('Output band.xyz and json files!')
        return(bs)


    def calc_band(self, r_wave, r_dens, dft_folder, dft101, kpts, bolt_factor):
        """
        Band calculation for DFTB+

        Parameters:
        r_wave, r_dens: the parameters of wavefunction and density
            Only used for label savefile here

        dft_folder: the directory of dft files
            Read dft json file from 
        
        dft101: dft six configurations, equivalent states, sorted by coordination.

        kpts: the kpoints for static calculation!
            should be much goood enough!

        """
        moles_n = []
        for num, mole in enumerate(dft101):
            os.system('rm charges.bin')
            config = mole.info['structure_name']
            print("\n#===========Electronic calculation for {}============#".format(config))
            print("bolt_factor:", bolt_factor[num])
            if bolt_factor[num] < 0.1:
                continue
            else:
        # ============================================================================ #
        #       # Static calculation
                configuration = mole.info['structure_name']
                print('Band structure:', configuration)

                mole.calc = self.dftbplus_calculator('static', mole, 'res', 'Yes', 'No', './', kpts)
                print('------------\nStep1: Static calculation\nEnergy:', mole.get_potential_energy())
                # mole.get_forces()
                fermi_static = mole.calc.get_fermi_level()
                print('Static Fermi_level:', fermi_static)


        # ============================================================================ #
        #       # Band structure calculation
                # path_dict = mole.cell.get_bravais_lattice().get_special_points()
                # kpts = ase.dft.kpoints.bandpath(path_dict, mole.cell)
                kpts_spe = self.path_dict[configuration]
                # mole.calc = self.dftbplus_calculator('band', mole, 'res', 'Yes', 'No', './',kpts_spe)

                kpts_path=self.get_bandpath_for_dftb(mole, kpts=kpts_spe)
                mole.calc = self.dftbplus_calculator('band', mole, 'res', 'Yes', 'No', './',kpts_path)

                # first No: we have repulsive potential in skf
                # second No: we drop SCC calculation
                print('------------\nStep2: Band structure calculation, \nEnergy:', mole.get_potential_energy())
                # forces = mole.get_forces()
                moles_n.append(mole)


                bs = mole.calc.band_structure()
                fermi_band = mole.calc.get_fermi_level()
                print("Band Fermi_level:", fermi_band, '\n')
                if abs(fermi_band - fermi_static)/abs(fermi_static) > 0.1:
                    print('Warning!!! The Fermi_level between static and band is not consistent more than 10%!!!')


                from ase.dft import bandgap
                band_gap = round(bandgap.bandgap(mole.calc)[0],3)
                print('Band gap from bandgap.bandgap(mole.calc):', band_gap, '\n')
                # bs._reference = fermi_static      #Fermi level from static calculation!!!

                # Save band structure
                if band_gap != 0.0: # calculate from dftb+, why don't use band of dft? since dftb+ can't be so roubust as to confuse conductor and insulator/semi-
                    elements = np.unique(mole.numbers)
                    elem_syms = [chemical_symbols[i] for i in elements] # get the element symbols ['Si']
                    tot_ele = self.atoms_info[elem_syms[0]]['atomic_number']
                    _, _, _, outer_ele = self.get_orbit(self.atoms_info[elem_syms[0]]['valence_shell']) # outer_ele is the num of valence electron
                    inner_ele = int(tot_ele - outer_ele)
                    num_atom = mole.get_global_number_of_atoms()


                    dftb_json_energies = bs.energies
                    lumo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2)].min()
                    homo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2) - 1].max()
                    band_from_json = round(lumo_e - homo_e,3) if lumo_e - homo_e > 0 else 0.0
                    print("lumo_e={:.4f}\nhomo_e={:.4f}\nDFTB_gap:{}".format(lumo_e, homo_e, band_from_json))
                    assert round(band_gap, 3) == round(band_from_json, 3), "The band gap of bandgap.bandgap() is not consistent with json file!"

                    # if round((lumo_e + homo_e)/2,4) == round(fermi_static,4):
                    print("WARNING: The Fermi level of DFTB might not be suitable for insulator and semiconductor, but I corrected it!")
                    origin_band = copy.deepcopy(bs)
                    origin_band._reference = fermi_static
                    origin_band.write(configuration + '_' + str(r_wave) +'_' +  str(r_dens) + '_o.json') # save the original band structure just for reference

                    bs._reference = homo_e

                    bs_dft = read_json(dft_folder / str(configuration + '.json'))
                    bs_dft_energies = bs_dft.energies
                    homo_e = bs_dft_energies[0, :, ceil(tot_ele * num_atom / 2) -1].max()
                    bs_dft._reference = homo_e
    
                    # else:
                    #     bs._reference = fermi_static
                    #     bs_dft = read_json(dft_folder / str(configuration + '.json'))
    
                else:
                    bs._reference = fermi_static
                    bs_dft = read_json(dft_folder / str(configuration + '.json'))
                    # dftb_json_energies = dftb_json.subtract_reference().energies


                bs.write(configuration + '_' + str(r_wave) +'_' +  str(r_dens) + '.json')
        #         emin = bs.subtract_reference().energies.min()
        #         emax = bs.subtract_reference().energies.max()
        #         emin, emax = -20, 20

                
        # # ============================================================================ #
        # # #     # Plot band structure
        #         fig, ax = plt.subplots(1,1, dpi=150)
        #         fig.suptitle("Parameters: r_wave={}, r_dens={}".format(r_wave, r_dens))
        #         ax.set_ylabel('Energies (eV)', fontsize=13)
        #         bs.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['g'])
        #         bs_dft.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['r'])
        #         fig.clf()
        print('Output band.xyz and json files!')
        ase.io.write('band.xyz', moles_n)
        return(moles_n)


    def vs_forces(self, dft_data, rep_filename):
        dft_forces = []
        dftb_forces = []
        num = 0

        # input dataset
        dft_dataset = dft_data
        dftb_dataset = ase.io.read(rep_filename + '@:')

        N = len(dft_dataset)

        for i in range(N):
            mole_dft = dft_dataset[i]
            mole_dftb = dftb_dataset[i]

            # get forces
            dft_force = mole_dft.get_forces()
            dftb_force = mole_dftb.get_forces()

            dft_forces.extend(dft_force.flatten())
            dftb_forces.extend(dftb_force.flatten())
            row = dft_force.shape[0]
            num += row * 3

        lines = np.arange(num)
        return(lines, dft_forces, dftb_forces)

    # def mk_traj(self, data: str, elements: int) -> None:
    def mk_traj(self, atoms: Atoms, traj_label: str, default_eos_points: int) -> None:
        '''
        Generate traj file for each configuration
        Input:
            dft_na: the name of dft file, ('dft.xyz')
        Output:
            0,1,2,3... traj
        '''


        num_atoms = int(len(atoms) / default_eos_points)
        for n in range(num_atoms):
            traj = Trajectory('{}_{}.traj'.format(traj_label, n), 'w')

            sub_atoms = atoms[n * default_eos_points: (n + 1) * default_eos_points]
            [traj.write(atoms) for atoms in sub_atoms]

            # for mole in range(default_eos_points):
            #     atoms = data[n_mole * default_eos_points + mole]  # read molecule from dft_data
            #     traj.write(atoms)


    def fit(self, na: str):
        '''
        Fit EOS based on Birch Murnaghan
        '''
        V = []
        E = []
        for atoms in ase.io.read('{}.traj@:'.format(na)):
            V.append(atoms.get_volume() / len(atoms))
            E.append(atoms.get_potential_energy() / len(atoms))
        eos = EOS(V, E, 'birchmurnaghan')
        eos.fit(warn=False)
        e0, B, Bp, v0 = eos.eos_parameters
        return e0, v0, B, Bp

    def eos_atoms(self, atoms_input: str, default_eos_points: int, calc_label: str):
        """
        
        """

        # if dftb_na is a string not a Atoms object
        if isinstance(atoms_input, str):
            dftb_atoms = ase.io.read(atoms_input, ':')
        elif isinstance(atoms_input, list):   #[Atoms, Atoms]
            dftb_atoms = atoms_input


        num_atoms = int(len(atoms_input) / default_eos_points)


        # Get atomic number
        atomic_numbers = []
        [atomic_numbers.extend(i.numbers) for i in dftb_atoms]
        atomic_numbers = np.unique(atomic_numbers)


        atomic_symbols = [chemical_symbols[i] for i in atomic_numbers]
        traj_label = f"eos_{'_'.join(atomic_symbols)}"


        # elements = 
        # num_atoms = len(dft_center)
        # lens = len(dftb_atoms)
        # num_atoms = int(lens / DEFAULT_EOS_POINTS)
        # elements = np.unique(dftb_atoms[0].numbers)
        # elem_sym = chemical_symbols[elements]

        self.mk_traj(dftb_atoms, traj_label, default_eos_points)

        data = {}  # Dict[str, Dict[str, float]]
        for n in range(num_atoms):
            na = traj_label + '_' + str(n)
            try:  # to avoid unexpected parameters/configurations
                e0, v0, B, Bp = self.fit(na)
            except RuntimeError:
                e0, v0, B, Bp = 1000, 1000, 1000, 1000
            data[na] = {f'{calc_label}_energy': e0,
                        f'{calc_label}_volume': v0,
                        f'{calc_label}_B': B,
                        f'{calc_label}_Bp': Bp}
            print(f"\n{na}\n{data[na]}")
            # print('\n' + na + '\n' + str(data[na]))
        Path(f'{calc_label}.json').write_text(json.dumps(data))
        return(data)

    # def eos_dftb(self, dftb_input: str, dft_center: Atoms):
    #     """
        
    #     """

    #     # if dftb_na is a string not a Atoms object
    #     if isinstance(dftb_input, str):
    #         dftb_atoms = ase.io.read(dftb_input, ':')
    #     elif isinstance(dftb_input, list):   #[Atoms, Atoms]
    #         dftb_atoms = dftb_input


    #     num_atoms = len(dft_center)
    #     # lens = len(dftb_atoms)
    #     # num_atoms = int(lens / DEFAULT_EOS_POINTS)
    #     elements = np.unique(dftb_atoms[0].numbers)[0]
    #     elem_sym = chemical_symbols[elements]

    #     self.mk_traj(dftb_atoms, elements)

    #     data = {}  # Dict[str, Dict[str, float]]
    #     for n in range(num_atoms):
    #         na = elem_sym + '_' + str(n)
    #         try:  # to avoid unexpected parameters/configurations
    #             e0, v0, B, Bp = self.fit(na)
    #         except RuntimeError:
    #             e0, v0, B, Bp = 1000, 1000, 1000, 1000
    #         data[na] = {'dftb_energy': e0,
    #                     'dftb_volume': v0,
    #                     'dftb_B': B,
    #                     'dftb_Bp': Bp}
    #         print('\n' + na + '\n' + str(data[na]))
    #     Path('dftb.json').write_text(json.dumps(data))
    #     return(data)

    def eos_cal(self, dataset: json, bolt_factor: list):
        """
        Calculate eos value

        Parameters:
            dataset: include dftb and dft data
            bolt_factor: bolt_factor of different configuration

        Return:
            delta_eos = (V_{0, DFTB} / V_{0, DFT} - 1)**2 + (B_{DFTB} / B_{DFT} - 1)**2
            mu = delta_eos * ((r_{cut} / r_{ref} - 1)**2 + 1)
        """

        # Write LogFile
        for name in ['volume', 'B', 'Bp']:
            with open(name + '.csv', 'w') as f:
                print('#symbol, dft, dftb', file=f)
                for symbol, dct in dataset.items():
                    values = [dct[code + '_' + name]
                              for code in ['dft', 'dftb']]
                    if name == 'B':
                        values = [val * 1e24 / kJ for val in values]
                    print('{},'.format(symbol), ', '.join(
                        '{:.2f}'.format(value) for value in values), file=f)

        # calculate mu_eos
        delta_tot, mu_v_tot, b_tot, n_i = 0, 0, 0, 0
        # n_i = 0  # for multiply boltzmann factor

        bolt_left = []
        with open('delta.csv', 'w') as f:
            print('#symbol,delta(dft-dftb),bolt_factor,delta*bolt', file=f)
            for symbol, dct in dataset.items():
                # Get v0, B, Bp:

                # Read data from json file
                dft, dftb = [(dct[code + '_volume'],
                              dct[code + '_B'],
                              dct[code + '_Bp'])
                              for code in ['dft', 'dftb']]
                
                # Convert units
                v0_dftb, b_dftb, v0_dft, b_dft = dftb[0], dftb[1] * \
                    1e24 / kJ, dft[0], dft[1] * 1e24 / kJ


                # Calculate LOSS function
                # If v0_dftb in 80% ~ 120% of v0_dft, we use it.

                # 1. Volume difference
                mu_v = abs(v0_dftb / v0_dft - 1)
                print(f'mu_v   =|{v0_dftb:16.3f} / {v0_dft:17.3f} -1| = {mu_v:10.5f}')
                if math.isnan(mu_v):  # To avoid unexpected parameters/configurations
                    mu_v = 100

                # 2. Bulk modulus difference
                try:
                    mu_logb = abs(np.log10(b_dftb) / np.log10(b_dft) - 1) + 1
                    print(f'mu_dftb=|log10({b_dftb:10.3f})/log10({b_dft:10.3f}) - 1| + 1 = {mu_logb:10.5f}\n')
                # Except unphysical log(b) value
                # B: bulk module will be deprecated in the future
                except ZeroDivisionError:
                    print('The birchmurnaghan EOS fitting for DFT failed')
                    mu_logb = 100
                if math.isnan(mu_logb):  # To avoid unexpected parameters/configurations
                    mu_logb = 100

                # 3. Multiplication between them (will be deprecated in the future)
                delta_eos = mu_v * mu_logb
                delta_eos_bolt_factor = delta_eos * bolt_factor[n_i]

                # Summary and Boltzmann factor additionif boltzmann factor larger >0.1, we consider its effects
                # otherwise, we neglect its influences
                if bolt_factor[n_i] < 0.1:
                    n_i += 1
                    continue
                else:
                    mu_v_tot += mu_v * bolt_factor[n_i]
                    b_tot += mu_logb * bolt_factor[n_i]
                    delta_tot += delta_eos_bolt_factor
                    bolt_left.append(bolt_factor[n_i])

                    print('{}, {:5.2f}, {:5.2f}, {:5.2f}'.format(
                        symbol, delta_eos, bolt_factor[n_i], delta_eos_bolt_factor), file=f)
                    n_i += 1
                    

                    # mu_v_tot  = mu_v_tot  / np.sum(bolt_left)
                    # delta_tot = delta_tot / np.sum(bolt_left)
                    # b_tot = b_tot / np.sum(bolt_left)
        return(delta_tot, mu_v_tot, b_tot)

    def delta_eos(self, dataset: json, bolt_factor: list):
        '''
        Calculate delta between AIMS and DFTB+
        '''

        for name in ['volume', 'B', 'Bp']:
            with open(name + '.csv', 'w') as f:
                print('# symbol, aims, dftb', file=f)
                for symbol, dct in dataset.items():
                    values = [dct[code + '_' + name]
                              for code in ['aims', 'dftb']]
                    if name == 'B':
                        values = [val * 1e24 / kJ for val in values]
                    print('{},'.format(symbol), ', '.join(
                        '{:.2f}'.format(value) for value in values), file=f)

        # calculate delta
        delta_tot = 0
        n_i = 0  # for multiply boltzmann factor
        with open('delta.csv', 'w') as f:
            print('# symbol, aims-dftb', 'bolt_factor', 'delta * bolt', file=f)
            for symbol, dct in dataset.items():
                # Get v0, B, Bp:
                aims, dftb = [(dct[code + '_volume'],
                               dct[code + '_B'],
                               dct[code + '_Bp'])
                              for code in ['aims', 'dftb']]

                delta_val = delta(*aims, *dftb) * 1000
                if math.isnan(delta_val):  # To avoid unexpected parameters/configurations
                    delta_val = 500
                delta_val_bolt_factor = delta_val * bolt_factor[n_i]
                n_i += 1
                delta_tot += delta_val_bolt_factor
                print('{}, {:5.2f}, {:5.2f}, {:5.2f}'.format(
                    symbol, delta_val, bolt_factor[n_i-1], delta_val_bolt_factor), file=f)
        return(delta_tot)
        
    def get_kpt_position(self, spe_kpts, kpts):
        """
        Get the position of the special points in the path.

        Parameters:
        spe_kpts: dict of special points
            eg. {'G': [0.0, 0.0, 0.0], 'K': [0.5, 0.5, 0.0], 'L': [0.5, 0.0, 0.5], 'U': [0.0, 0.5, 0.5], 'W': [0.5, 0.5, 0.5], 'X': [0.5, 0.5, 0.5]}
        kpts: ndarray of kpoints, shape=(nkpt, 3)

        Returns:
        kpt_pos: dict of kpoint indexes by special points
            eg. {'G': 40, 'K': 80, 'L': 60, 'W': 0, 'X': 20}
        """
        kpt_pos = {}
        for special_point in spe_kpts.keys():
            # print(special_point)
            for num, kpt in enumerate(kpts):
                if list(np.around(spe_kpts[special_point],6)) == list(np.around(kpt,6)):
                    # print(spe_kpts[special_point], kpt)
                    kpt_pos[special_point] = num
                    break
        return kpt_pos
    


    def delta_band(self, r_wave, r_dens, dft_folder, dft101, bolt_factor):
        '''
        Calculate delta between AIMS and DFTB+

        Parameters:
        r_wave, r_dens: the parameters of wavefunction and density
            Only used for label savefile here

        dft_directory: directory of DFT
            eg. '../../'

        dft101: dft six configurations, equivalent states, sorted by coordination.

        bolt_factor: list of boltzmann factor
            eg. [0.0, 0.0, 0.0, 0.0,0.86,  0.13]
        
        '''
        # odft101 = ase.io.read(dft_directory + '101.xyz',':')
        # dft101 = [odft101[0], odft101[5], odft101[4], odft101[3], odft101[2], odft101[1]]

        elements = np.unique(dft101[0].numbers)
        elem_syms = [chemical_symbols[i] for i in elements] # get the element symbols ['Si']

        # bolt_factor = {'dimer':0.0, 'AA-graphite':0.0, 'diamond': 0.0, 'beta-Sn': 0.0, 'bcc':0.86, 'fcc': 0.13}

        tot_ele = self.atoms_info[elem_syms[0]]['atomic_number']
        _, _, _, outer_ele = self.get_orbit(self.atoms_info[elem_syms[0]]['valence_shell']) # outer_ele is the num of valence electron
        inner_ele = int(tot_ele - outer_ele)
        print("inner_ele:", inner_ele, "outer_ele:", outer_ele)


        dft_info = {'dimer':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'diamond':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'beta-Sn':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'bcc':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'fcc':{'special_points':'', 'energies':'', 'bolt_factor':''}}
        dftb_info = {'dimer':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'diamond':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'beta-Sn':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'bcc':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'fcc':{'special_points':'', 'energies':'', 'bolt_factor':''}}

        kband_info={}
        delta_band = []
        delta_homos, delta_lumos = [], []
        delta_fermi_homos, delta_fermi_lumos = [], []
        bolt_left = []


        for n, mole in enumerate(dft101):
            config = mole.info['structure_name']
            print("\n#===========Delta band structure for {}===========#".format(config))
            print(config, "\nbolt_factor:", bolt_factor[n])
            # if bolt_factor > 0.1
            if bolt_factor[n] < 0.1:
                delta_homos.append(0.0)
                delta_lumos.append(0.0)
                delta_fermi_homos.append(0.0)
                delta_fermi_lumos.append(0.0)
                continue
            else:
                num_atom = mole.get_global_number_of_atoms()
                dft_band_energies = np.empty(shape=(0, ceil(outer_ele/2) * num_atom))
                dftb_band_energies = np.empty(shape=(0, ceil(outer_ele/2) * num_atom))

            # ============================================================================ #
            # # 1. read information

                # do a check for dft_json, in order to keep fermi level always loacated max(homo) for insulator and semi-conductor
                dft_json = read_json(dft_folder / str(config+'.json'))
                dft_json_check = dft_json.energies
                check_homo_e = dft_json_check[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom - 1]
                check_lumo_e = dft_json_check[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom]
                dft_bandgap = min(check_lumo_e) - max(check_homo_e) if min(check_lumo_e) - max(check_homo_e) > 0.0 else 0.0
                if dft_bandgap > 0.0: # insulator or semi-conductor
                    dft_json._reference = max(check_homo_e)
                

                dft_json_energies = dft_json.subtract_reference().energies
                dftb_json = read_json(config  +'_' + str(r_wave) +'_' +  str(r_dens) + '.json')
                dftb_json_energies = dftb_json.subtract_reference().energies

                
                # print("Dftb_fermi(after correction):", 0.0)
                # lumo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2)].min()
                # homo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2) - 1].max()
                # print("lumo_e={:.4f}\nhomo_e={:.4f}\nDFTB_gap:{}".format(lumo_e, homo_e, lumo_e - homo_e))
                # if round(lumo_e + homo_e,4) == 0.0:
                #     print("WARNING: The Fermi level of DFTB is not corrected, but I corrected it!")
                #     fermi_update = dftb_json.reference + homo_e
                #     dftb_json._reference = fermi_update
                #     dftb_json_energies = dftb_json.subtract_reference().energies

            # ============================================================================ #
            # # 2. get special points and index

                dft_spe_kpts, dft_kpts = dft_json.path.special_points, dft_json.path.kpts
                dftb_spe_kpts, dftb_kpts = dftb_json.path.special_points, dftb_json.path.kpts
                print("\nDFT_SPECIAL_KPTS:")
                pp.pprint(dft_spe_kpts)
                print("\nDFTB_SPECIAL_KPTS:")
                pp.pprint(dftb_spe_kpts)


                dft_kpt_idx = self.get_kpt_position(dft_spe_kpts, dft_kpts)
                dftb_kpt_idx = self.get_kpt_position(dftb_spe_kpts, dftb_kpts)
                # print("\nThe Index:", dft_kpt_idx, dftb_kpt_idx)
                

            # ============================================================================ #
            # # 3. get energies
                
                dft_homo_e = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom - 1]
                dftb_homo_e = dftb_json_energies[0, :, ceil(outer_ele/2)*num_atom - 1]
                dft_homo_rel_e = dft_homo_e - max(dft_homo_e)
                dftb_homo_rel_e = dftb_homo_e - max(dftb_homo_e)


                dft_lumo_e = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom]
                dftb_lumo_e = dftb_json_energies[0, :, ceil(outer_ele/2)*num_atom]
                dft_lumo_rel_e = dft_lumo_e - min(dft_lumo_e)
                dftb_lumo_rel_e = dftb_lumo_e - min(dftb_lumo_e)


                # save relative energies < +-1 eV to a new list
                dft_homo_ee, dftb_homo_ee = [], []
                dft_lumo_ee, dftb_lumo_ee = [], []
                dft_fermi_homo_ee, dftb_fermi_homo_ee = [], []
                dft_fermi_lumo_ee, dftb_fermi_lumo_ee = [], []


                for nn, ee in enumerate(dft_homo_e):
                    if abs(ee) <= 1.0:
                        dft_fermi_homo_ee.append(ee)
                        dftb_fermi_homo_ee.append(dftb_homo_e[nn])
                
                for nn, ee in enumerate(dft_lumo_e):
                    if abs(ee) <= 1.0:
                        dft_fermi_lumo_ee.append(ee)
                        dftb_fermi_lumo_ee.append(dftb_homo_e[nn])
                    else:
                        dft_fermi_lumo_ee.append(0.0)
                        dftb_fermi_lumo_ee.append(0.0)


                for nn, ee in enumerate(dft_homo_rel_e):
                    if abs(ee) <= 1.0:
                        dft_homo_ee.append(ee)
                        dftb_homo_ee.append(dftb_homo_rel_e[nn])
                
                for nn, ee in enumerate(dft_lumo_rel_e):
                    if abs(ee) <= 1.0:
                        dft_lumo_ee.append(ee)
                        dftb_lumo_ee.append(dftb_lumo_rel_e[nn])


        # ============================================================================ #
        # #     # Plot band structure
                emin, emax = -20, 20
                fig, ax = plt.subplots(1,1, dpi=150)
                fig.suptitle("Parameters: r_wave={}, r_dens={}".format(r_wave, r_dens))
                ax.set_ylabel('Energies (eV)', fontsize=13)
                dftb_json.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=config+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['g'])
                dft_json.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=config+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['r'])
                fig.clf()                

                # for Gamma in dft_kpt_idx.keys():
                #     dft_e = dft_json_energies[0, dft_kpt_idx[Gamma], ceil(inner_ele/2)*num_atom: ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom]
                #     dftb_e = dftb_json_energies[0, dftb_kpt_idx[Gamma], 0: ceil(outer_ele/2)*num_atom]
                
                #     dft_band_energies = np.vstack((dft_band_energies, dft_e))
                #     dftb_band_energies = np.vstack((dftb_band_energies, dftb_e))
                # # dft_homo_e = dft_e[ceil(outer_ele * num_atom / 2) - 1 ,:]
                # # dftb_homo_e = dftb_e[ceil(outer_ele * num_atom / 2) - 1 ,:]
                # dft_homo_e = dft_band_energies[:,-1] 
                # dftb_homo_e = dftb_band_energies[:,-1]
                # dft_homo_e_rel = dft_homo_e - max(dft_homo_e)  # relative to the highest of homo
                # dftb_homo_e_rel = dftb_homo_e - max(dftb_homo_e)

                # # constrain the loss within 1 eV
                # dft_ee, dftb_ee = [], []
                # for nn, ee in enumerate(dft_homo_e_rel):
                #     if abs(ee) <=1.0:
                #         dft_ee.append(ee)
                #         dftb_ee.append(dftb_homo_e_rel[nn])

                # dft_homo_e_rel = np.array(dft_ee)
                # dftb_homo_e_rel = np.array(dftb_ee)
            # ============================================================================ #
            # # 4. get band gap
                # bg_dft = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom + 1].min() - dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom].max()
                # bg_dftb = dftb_json_energies[0, :, ceil(outer_ele/2 + 1)*num_atom].min() - dftb_json_energies[0, :, ceil(outer_ele/2)*num_atom].max()
                # print("DFT band gap:", bg_dft, "\nDFTB band gap:", bg_dftb)


            # ============================================================================ #
            # # 4. save information

                # assert dft_kpt_idx.keys() == dftb_kpt_idx.keys()
                dft_info[config]['special_points'] = ' '.join(dft_kpt_idx.keys())
                dft_info[config]['energies'] = dft_band_energies
                dft_info[config]['bolt_factor'] = bolt_factor[n]

                dftb_info[config]['special_points'] = ' '.join(dftb_kpt_idx.keys())
                dftb_info[config]['energies'] = dftb_band_energies
                dftb_info[config]['bolt_factor'] = bolt_factor[n]

                delta_energies = np.mean(np.abs(np.array(dft_band_energies) - np.array(dftb_band_energies))) * bolt_factor[n]

                delta_homo = np.mean(np.abs(np.array(dft_homo_ee).flatten() - np.array(dftb_homo_ee).flatten())) * bolt_factor[n]
                delta_lumo = np.mean(np.abs(np.array(dft_lumo_ee).flatten() - np.array(dftb_lumo_ee).flatten())) * bolt_factor[n]

                delta_fermi_homo = np.mean(np.abs(np.array(dft_fermi_homo_ee).flatten() - np.array(dftb_fermi_homo_ee).flatten())) * bolt_factor[n]
                delta_fermi_lumo = np.mean(np.abs(np.array(dft_fermi_lumo_ee).flatten() - np.array(dftb_fermi_lumo_ee).flatten())) * bolt_factor[n]
                
                # delta_band.append(delta_energies)
                delta_homos.append(delta_homo)
                delta_lumos.append(delta_lumo)

                delta_fermi_homos.append(delta_fermi_homo)
                delta_fermi_lumos.append(delta_fermi_lumo)

                bolt_left.append(bolt_factor[n])

            # bs_dft = read_json(dft_folder / str(configuration + '.json'))
            # ============================================================================ #
            # # 5. plot band structure
                # emin, emax = -20, 20
                # fig, ax = plt.subplots(1,1, dpi=150)
                # fig.suptitle("Parameters: r_wave={}, r_dens={}".format(r_wave, r_dens))
                # ax.set_ylabel('Energies (eV)', fontsize=13)
                # bs.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['g'])
                # bs_dft.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['r'])
                # fig.clf()

        kband_info = {'dft':dft_info, 'dftb':dftb_info, 'delta':np.mean(delta_homos)}
        # mu_band = np.sum(delta_band) / np.sum(bolt_left)
        # delta_homos = np.sum(delta_homo) / np.sum(bolt_left)
        # mu_band_homo = np.sum(delta_homos) / np.sum(bolt_left)  # homo_rel
        # mu_band_lumo = np.sum(delta_lumos) / np.sum(bolt_left)    # lumo_rel

        mu_band_homo = np.sum(delta_fermi_homos) / np.sum(bolt_left)
        mu_band_lumo = np.sum(delta_fermi_lumos) / np.sum(bolt_left)

        with open('kband_info' + '_' + str(r_wave) +'_' +  str(r_dens) + '.pkl', 'wb') as f:
            pickle.dump(kband_info, f)
        return kband_info, delta_fermi_homos, delta_fermi_lumos, mu_band_homo, mu_band_lumo


    # def delta_band_binary(self, para_list, json_file, atoms_list, bolt_factor):

    def delta_band_binary(self, conf_parameters, json_file, dft_atoms, bolt_f):
        '''
        Calculate delta between AIMS and DFTB+

        Parameters:
        r_wave, r_dens: the parameters of wavefunction and density
            Only used for label savefile here

        dft_directory: directory of DFT
            eg. '../../'

        dft101: dft six configurations, equivalent states, sorted by coordination.

        bolt_factor: list of boltzmann factor weight
            eg. [0.0, 0.0, 0.0, 0.0,0.86,  0.13]
        
        '''

        # dft_info = {'DFT':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''}}
        # dftb_info = {'DFTB':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''}}

        # kband_info={}
        # delta_band = []
        delta_homos, delta_lumos = [], []
        delta_fermi_homos, delta_fermi_lumos = [], []
        bolt_left = []


        for n, mole in enumerate(dft_atoms):
            config = mole.info['structure_name']
            print("\n#===========Delta band structure for {}===========#".format(config))
            print(config, "\nbolt_factor:", bolt_f[n])

            if bolt_f[n] < 0.1:
                delta_homos.append(0.0)
                delta_lumos.append(0.0)
                delta_fermi_homos.append(0.0)
                delta_fermi_lumos.append(0.0)
                continue


            else:
                # Get the valence electron info for DFTB and DFT
                dftb_tot_chemical_symbols = mole.get_chemical_symbols()
                dftb_num_valence_electron = np.sum(self.get_orbit(self.atoms_info[syms]['valence_shell'])[3] for syms in dftb_tot_chemical_symbols)


                dft_tot_chemical_symbols = mole.get_chemical_symbols()
                dft_num_valence_electron = np.sum(self.get_orbit(self.atoms_info_qe[syms]['valence_shell'])[3] for syms in dft_tot_chemical_symbols)

                # dft_band_energies = np.empty(shape=(0, ceil(outer_ele/2) * num_binary))
                # dft_band_energies = np.empty(shape=(0, ceil(dft_num_valence_electron/2)))


                # dftb_band_energies = np.empty(shape=(0, ceil(dftb_num_valence_electron/2)))

            # ============================================================================ #
            # # 1. read information

                # do a check for dft_json, in order to keep fermi level always loacated max(homo) for insulator and semi-conductor
                dft_json = read_json(json_file)
                dft_json_check = dft_json.energies

                # outer_ele_dft, virtual_ele_dft = ceil(outer_ele/2) * num_binary , 4
                # outer_ele_dft, virtual_ele_dft = ceil(outer_ele_qe/2) * num_binary , 4

                # check_homo_e = dft_json_check[0, :, ceil(inner_ele_dft/2)*num_binary + ceil(outer_ele_dft/2)*num_binary - 1]
                # check_lumo_e = dft_json_check[0, :, ceil(inner_ele_dft/2)*num_binary + ceil(outer_ele_dft/2)*num_binary]

                # Make sure find the right band
                dft_lumo_e_check = dft_json_check[0, :, ceil(dft_num_valence_electron/2)]
                dft_homo_e_check = dft_json_check[0, :, ceil(dft_num_valence_electron/2) - 1]


                dft_bandgap = min(dft_lumo_e_check) - max(dft_homo_e_check) if min(dft_lumo_e_check) - max(dft_homo_e_check) > 0.0 else 0.0
                if dft_bandgap > 0.0: # insulator or semi-conductor
                    dft_json._reference = max(dft_homo_e_check)

                # After correction
                dft_json_energies = dft_json.subtract_reference().energies
                dft_lumo_e = dft_json_energies[0, :, ceil(dft_num_valence_electron/2)]
                dft_homo_e = dft_json_energies[0, :, ceil(dft_num_valence_electron/2) - 1]


                # Read DFTB JSON
                para_name = '_'.join([str(round(i,3)) for i in conf_parameters])
                dftb_json = read_json(config  + '_' + para_name  + '.json')
                dftb_json_energies = dftb_json.subtract_reference().energies

            # ============================================================================ #
            # # 2. get special points and index

                dft_spe_kpts, dft_kpts = dft_json.path.special_points, dft_json.path.kpts
                dftb_spe_kpts, dftb_kpts = dftb_json.path.special_points, dftb_json.path.kpts
                print("\nDFT_SPECIAL_KPTS:")
                pp.pprint(dft_spe_kpts)
                print("\nDFTB_SPECIAL_KPTS:")
                pp.pprint(dftb_spe_kpts)
                assert dft_spe_kpts.keys() == dftb_spe_kpts.keys(), "The special points are not the same for DFT and DFTB"

                # dft_kpt_idx = self.get_kpt_position(dft_spe_kpts, dft_kpts)
                # dftb_kpt_idx = self.get_kpt_position(dftb_spe_kpts, dftb_kpts)
                # assert dft_kpt_idx == dftb_kpt_idx, "The special points are not the same for DFT and DFTB"
                # print("\nThe Index:", dft_kpt_idx, dftb_kpt_idx)
                

            # ============================================================================ #
            # # 3. get energies
                
                # dft_homo_e = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom - 1]
                dftb_lumo_e = dftb_json_energies[0, :, ceil(dftb_num_valence_electron/2)]
                dftb_homo_e = dftb_json_energies[0, :, ceil(dftb_num_valence_electron/2) - 1]

                # Plot only homo and lumo
                emin, emax = -10, 10
                fig, ax = plt.subplots(1,1, dpi=80)
                fig.suptitle("Parameters:" + str(para_name))
                xx = np.linspace(0,1,len(dft_lumo_e))
                ax.plot(xx, dft_lumo_e, color='r', linestyle='-', label='DFT')
                ax.plot(xx, dft_homo_e, color='r', linestyle='--')
                ax.plot(xx, dftb_lumo_e, color='g', linestyle='-', label='DFTB')
                ax.plot(xx, dftb_homo_e, color='g', linestyle='--')

                ax.set_ylim(emin, emax)
                ax.legend()
                fig.tight_layout()
                fig.savefig('pic_' + para_name + '.png')
                fig.clf()


                # save relative energies < +-1 eV to a new list
                dft_fermi_homo_ee, dftb_fermi_homo_ee = [], []
                dft_fermi_lumo_ee, dftb_fermi_lumo_ee = [], []

                for nn, ee in enumerate(dft_homo_e):
                    if abs(ee) <= 1.0:
                        dft_fermi_homo_ee.append(ee)
                        dftb_fermi_homo_ee.append(dftb_homo_e[nn])

                
                for nn, ee in enumerate(dft_lumo_e):
                    if abs(ee) <= 1.0:
                        dft_fermi_lumo_ee.append(ee)
                        dftb_fermi_lumo_ee.append(dftb_lumo_e[nn])
                    # else:
                    #     dft_fermi_lumo_ee.append(0.0)
                    #     dftb_fermi_lumo_ee.append(0.0)

                delta_fermi_homo = np.mean(np.abs(np.array(dft_fermi_homo_ee).flatten() - np.array(dftb_fermi_homo_ee).flatten())) * bolt_f[n]
                delta_fermi_lumo = np.mean(np.abs(np.array(dft_fermi_lumo_ee).flatten() - np.array(dftb_fermi_lumo_ee).flatten())) * bolt_f[n]

                delta_fermi_homos.append(delta_fermi_homo)
                delta_fermi_lumos.append(delta_fermi_lumo)

                bolt_left.append(bolt_f[n])


                # Not really use here
                # dft_homo_ee, dftb_homo_ee = [], []
                # dft_lumo_ee, dftb_lumo_ee = [], []
                # for nn, ee in enumerate(dft_homo_rel_e):
                #     if abs(ee) <= 1.0:
                #         dft_homo_ee.append(ee)
                #         dftb_homo_ee.append(dftb_homo_rel_e[nn])
                
                # for nn, ee in enumerate(dft_lumo_rel_e):
                #     if abs(ee) <= 1.0:
                #         dft_lumo_ee.append(ee)
                #         dftb_lumo_ee.append(dftb_lumo_rel_e[nn])

                # delta_homo = np.mean(np.abs(np.array(dft_homo_ee).flatten() - np.array(dftb_homo_ee).flatten())) * bolt_factor[n]
                # delta_lumo = np.mean(np.abs(np.array(dft_lumo_ee).flatten() - np.array(dftb_lumo_ee).flatten())) * bolt_factor[n]



            # ============================================================================ #
            # # 4. save information

                # assert dft_kpt_idx.keys() == dftb_kpt_idx.keys()
                # dft_info['DFT']['special_points'] = ' '.join(dft_kpt_idx.keys())
                # dft_info['DFT']['energies'] = dft_band_energies
                # dft_info['DFT']['bolt_factor'] = bolt_factor[n]

                # dftb_info['DFTB']['special_points'] = ' '.join(dftb_kpt_idx.keys())
                # dftb_info['DFTB']['energies'] = dftb_band_energies
                # dftb_info['DFTB']['bolt_factor'] = bolt_factor[n]

                # delta_energies = np.mean(np.abs(np.array(dft_band_energies) - np.array(dftb_band_energies))) * bolt_factor[n]

                
                # delta_band.append(delta_energies)
                # delta_homos.append(delta_homo)
                # delta_lumos.append(delta_lumo)



        # kband_info = {'dft':dft_info, 'dftb':dftb_info, 'delta':np.mean(delta_homos)}
        # mu_band = np.sum(delta_band) / np.sum(bolt_left)
        # delta_homos = np.sum(delta_homo) / np.sum(bolt_left)
        # mu_band_homo = np.sum(delta_homos) / np.sum(bolt_left)  # homo_rel
        # mu_band_lumo = np.sum(delta_lumos) / np.sum(bolt_left)    # lumo_rel

        mu_band_homo = np.sum(delta_fermi_homos) / np.sum(bolt_left)
        mu_band_lumo = np.sum(delta_fermi_lumos) / np.sum(bolt_left)

        # with open('kband_info' + '_' + str(round(r_wave_zn,3)) +'_' +  str(round(r_dens_zn,3)) +'_'+ str(round(r_wave_o,3)) +'_' +  str(round(r_dens_o,3))  + '.pkl', 'wb') as f:
        #     pickle.dump(kband_info, f)
        return mu_band_homo, mu_band_lumo


    def get_curvature(self, r_wave, r_dens, dft_folder, dft101, bolt_factor):
        '''
        Calculate delta between AIMS and DFTB+

        Parameters:
        r_wave, r_dens: the parameters of wavefunction and density
            Only used for label savefile here

        dft_directory: directory of DFT
            eg. '../../'

        dft101: dft six configurations, equivalent states, sorted by coordination.

        bolt_factor: list of boltzmann factor
            eg. [0.0, 0.0, 0.0, 0.0,0.86,  0.13]
        
        '''
        # odft101 = ase.io.read(dft_directory + '101.xyz',':')
        # dft101 = [odft101[0], odft101[5], odft101[4], odft101[3], odft101[2], odft101[1]]

        elements = np.unique(dft101[0].numbers)
        elem_syms = [chemical_symbols[i] for i in elements] # get the element symbols ['Si']

        # bolt_factor = {'dimer':0.0, 'AA-graphite':0.0, 'diamond': 0.0, 'beta-Sn': 0.0, 'bcc':0.86, 'fcc': 0.13}

        tot_ele = self.atoms_info[elem_syms[0]]['atomic_number']
        _, _, _, outer_ele = self.get_orbit(self.atoms_info[elem_syms[0]]['valence_shell']) # outer_ele is the num of valence electron
        inner_ele = int(tot_ele - outer_ele)
        print("inner_ele:", inner_ele, "outer_ele:", outer_ele)


        dft_info = {'dimer':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'diamond':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'beta-Sn':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'bcc':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'fcc':{'special_points':'', 'energies':'', 'bolt_factor':''}}
        dftb_info = {'dimer':{'special_points':'', 'energies':'', 'bolt_factor':''},   'AA-graphite':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'diamond':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'beta-Sn':{'special_points':'', 'energies':'', 'bolt_factor':''},
                    'bcc':{'special_points':'', 'energies':'', 'bolt_factor':''}, 'fcc':{'special_points':'', 'energies':'', 'bolt_factor':''}}

        kband_info={}
        delta_band = []
        delta_homos = []
        bolt_left = []
        delta_curvatures = []


        for n, mole in enumerate(dft101):
            config = mole.info['structure_name']
            print("\n#===========Delta band structure for {}===========#".format(config))
            print(config, "\nbolt_factor:", bolt_factor[n])
            # if bolt_factor > 0.1
            if bolt_factor[n] < 0.1:
                delta_homos.append(0.0)
                continue
            else:
                num_atom = mole.get_global_number_of_atoms()
                dft_band_energies = np.empty(shape=(0, ceil(outer_ele/2) * num_atom))
                dftb_band_energies = np.empty(shape=(0, ceil(outer_ele/2) * num_atom))

            # ============================================================================ #
            # # 1. read information

                dft_json = read_json(dft_folder / str(config+'.json'))
                dftb_json = read_json('dat/' + config  +'_' + str(r_wave) +'_' +  str(r_dens) + '.json')
                dft_json_energies = dft_json.subtract_reference().energies
                dftb_json_energies = dftb_json.subtract_reference().energies
                
                print("Dftb_fermi(after correction):", 0.0)
                lumo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2)].min()
                homo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2) - 1].max()
                print("lumo_e={:.4f}\nhomo_e={:.4f}\nDFTB_gap:{}".format(lumo_e, homo_e, lumo_e - homo_e))
                if round(lumo_e + homo_e,4) == 0.0:
                    print("WARNING: The Fermi level of DFTB is not corrected, but I corrected it!")
                    fermi_update = dftb_json.reference + homo_e
                    dftb_json._reference = fermi_update
                    dftb_json_energies = dftb_json.subtract_reference().energies

            # ============================================================================ #
            # # 2. get special points and index

                dft_spe_kpts, dft_kpts = dft_json.path.special_points, dft_json.path.kpts
                dftb_spe_kpts, dftb_kpts = dftb_json.path.special_points, dftb_json.path.kpts
                print("\nDFT_SPECIAL_KPTS:")
                pp.pprint(dft_spe_kpts)
                print("\nDFTB_SPECIAL_KPTS:")
                pp.pprint(dftb_spe_kpts)


                dft_kpt_idx = self.get_kpt_position(dft_spe_kpts, dft_kpts)
                dftb_kpt_idx = self.get_kpt_position(dftb_spe_kpts, dftb_kpts)
                # print("\nThe Index:", dft_kpt_idx, dftb_kpt_idx)
                

            # ============================================================================ #
            # # 3. get energies
                delta_curva_kpts = []
                for Gamma in dft_kpt_idx.keys():
                    dft_e = dft_json_energies[0, dft_kpt_idx[Gamma], ceil(inner_ele/2)*num_atom: ceil(inner_ele/2)*num_atom+ceil(outer_ele/2)*num_atom]
                    dftb_e = dftb_json_energies[0, dftb_kpt_idx[Gamma], 0: ceil(outer_ele/2)*num_atom]
                
                    dft_band_energies = np.vstack((dft_band_energies, dft_e))
                    dftb_band_energies = np.vstack((dftb_band_energies, dftb_e))



                    # get curvature
                    dftb_homo_e = dftb_json_energies[0, :, ceil((outer_ele*num_atom)/2) - 1]
                    dft_homo_e = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil((outer_ele*num_atom)/2) - 1]
                    # collect two points around kpt_idx to calculate the curvature
                    if dftb_kpt_idx[Gamma] == 0:
                        assert dft_kpt_idx[Gamma] == 0, "The end kpt_idx of DFT and DFTB are not the same!"
                        dftb_cpoints = [dftb_homo_e[dftb_kpt_idx[Gamma]], dftb_homo_e[dftb_kpt_idx[Gamma] + 3], dftb_homo_e[dftb_kpt_idx[Gamma]+6]]
                        dftb_curva = ((dftb_cpoints[2] - dftb_cpoints[1]) - (dftb_cpoints[1] - dftb_cpoints[0]))/2

                        dft_cpoints = [dft_homo_e[dft_kpt_idx[Gamma]], dft_homo_e[dft_kpt_idx[Gamma]+2], dft_homo_e[dft_kpt_idx[Gamma]+4]]
                        dft_curva = ((dft_cpoints[2] - dft_cpoints[1]) - (dft_cpoints[1] - dft_cpoints[0]))/2

                        if np.sign(dftb_curva) == np.sign(dft_curva):
                            delta_curva = abs(dft_curva - dftb_curva)
                        else:
                            delta_curva = abs(dft_curva - dftb_curva) * 10
                        delta_curva_kpts.append(delta_curva)
                        print("Gamma:{:10}dft_curva:{:12.5f}dftb_curva:{:12.5f}dftb_curva:{:12.5f}".format(Gamma, dft_curva, dftb_curva, delta_curva))

                    elif dftb_kpt_idx[Gamma] == len(dftb_kpts) - 1:
                        assert dft_kpt_idx[Gamma] == len(dft_kpts) - 1, "The end kpt_idx of DFT and DFTB are not the same!"
                        dftb_cpoints = [dftb_homo_e[dftb_kpt_idx[Gamma]-6], dftb_homo_e[dftb_kpt_idx[Gamma]-3], dftb_homo_e[dftb_kpt_idx[Gamma]]]
                        dftb_curva = ((dftb_cpoints[2] - dftb_cpoints[1]) - (dftb_cpoints[1] - dftb_cpoints[0]))/2

                        dft_cpoints = [dft_homo_e[dft_kpt_idx[Gamma]-4], dft_homo_e[dft_kpt_idx[Gamma]-2], dft_homo_e[dft_kpt_idx[Gamma]]]
                        dft_curva = ((dft_cpoints[2] - dft_cpoints[1]) - (dft_cpoints[1] - dft_cpoints[0]))/2

                        if np.sign(dftb_curva) == np.sign(dft_curva):
                            delta_curva = abs(dft_curva - dftb_curva)
                        else:
                            delta_curva = abs(dft_curva - dftb_curva) * 10
                        delta_curva_kpts.append(delta_curva)
                        print("Gamma:{:10}dft_curva:{:12.5f}dftb_curva:{:12.5f}dftb_curva:{:12.5f}".format(Gamma, dft_curva, dftb_curva, delta_curva))


                    else:                
                        dftb_cpoints = [dftb_homo_e[dftb_kpt_idx[Gamma]-10], dftb_homo_e[dftb_kpt_idx[Gamma]], dftb_homo_e[dftb_kpt_idx[Gamma]+10]]
                        dftb_curva = ((dftb_cpoints[2] - dftb_cpoints[1]) - (dftb_cpoints[1] - dftb_cpoints[0]))/2

                        dft_cpoints = [dft_homo_e[dft_kpt_idx[Gamma]-3], dft_homo_e[dft_kpt_idx[Gamma]], dft_homo_e[dft_kpt_idx[Gamma]+3]]
                        dft_curva = ((dft_cpoints[2] - dft_cpoints[1]) - (dft_cpoints[1] - dft_cpoints[0]))/2

                        if np.sign(dftb_curva) == np.sign(dft_curva):
                            delta_curva = abs(dft_curva - dftb_curva)
                        else:
                            delta_curva = abs(dft_curva - dftb_curva) * 10
                        delta_curva_kpts.append(delta_curva)
                        print("Gamma:{:10}dft_curva:{:12.5f}dftb_curva:{:12.5f}dftb_curva:{:12.5f}".format(Gamma, dft_curva, dftb_curva, delta_curva))


                # dft_homo_e = dft_e[ceil(outer_ele * num_atom / 2) - 1 ,:]
                # dftb_homo_e = dftb_e[ceil(outer_ele * num_atom / 2) - 1 ,:]
                dft_homo_e = dft_band_energies[:,-1] 
                dftb_homo_e = dftb_band_energies[:,-1]
                dft_homo_e_rel = dft_homo_e - max(dft_homo_e)
                dftb_homo_e_rel = dftb_homo_e - max(dftb_homo_e)
            # ============================================================================ #
            # # 4. get band gap
                # bg_dft = dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom + 1].min() - dft_json_energies[0, :, ceil(inner_ele/2)*num_atom + ceil(outer_ele/2)*num_atom].max()
                # bg_dftb = dftb_json_energies[0, :, ceil(outer_ele/2 + 1)*num_atom].min() - dftb_json_energies[0, :, ceil(outer_ele/2)*num_atom].max()
                # print("DFT band gap:", bg_dft, "\nDFTB band gap:", bg_dftb)


            # ============================================================================ #
            # # 4. save information

                assert dft_kpt_idx.keys() == dftb_kpt_idx.keys()
                dft_info[config]['special_points'] = ' '.join(dft_kpt_idx.keys())
                dft_info[config]['energies'] = dft_band_energies
                dft_info[config]['bolt_factor'] = bolt_factor[n]

                dftb_info[config]['special_points'] = ' '.join(dftb_kpt_idx.keys())
                dftb_info[config]['energies'] = dftb_band_energies
                dftb_info[config]['bolt_factor'] = bolt_factor[n]

                delta_energies = np.mean(np.abs(np.array(dft_band_energies) - np.array(dftb_band_energies))) * bolt_factor[n]

                delta_homo = np.mean(np.abs(dft_homo_e_rel - dftb_homo_e_rel)) * bolt_factor[n]
                delta_curva_config = np.mean(delta_curva_kpts) * bolt_factor[n] # this config
                
                delta_band.append(delta_energies)
                delta_homos.append(delta_homo)
                bolt_left.append(bolt_factor[n])
                delta_curvatures.append(delta_curva_config)

            # bs_dft = read_json(dft_folder / str(configuration + '.json'))
            # ============================================================================ #
            # # 5. plot band structure
                # emin, emax = -20, 20
                # fig, ax = plt.subplots(1,1, dpi=150)
                # fig.suptitle("Parameters: r_wave={}, r_dens={}".format(r_wave, r_dens))
                # ax.set_ylabel('Energies (eV)', fontsize=13)
                # bs.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['g'])
                # bs_dft.subtract_reference().plot(ax=ax, emin=emin, emax=emax, filename=configuration+'_' + str(r_wave) +'_' +  str(r_dens) + '.png', colors=['r'])
                # fig.clf()

        kband_info = {'dft':dft_info, 'dftb':dftb_info, 'delta':np.mean(delta_band)}
        # mu_band = np.sum(delta_band) / np.sum(bolt_left)
        # delta_homos = np.sum(delta_homo) / np.sum(bolt_left)
        mu_band = np.mean(delta_homos) / np.sum(bolt_left)
        mu_curvatures = np.sum(delta_curvatures) / np.sum(bolt_left)

        with open('kband_info' + '_' + str(r_wave) +'_' +  str(r_dens) + '.pkl', 'wb') as f:
            pickle.dump(kband_info, f)
        return kband_info, delta_band, delta_homos, mu_band, delta_curvatures, mu_curvatures


    def plot_vs(self, symbol, parameters, dft, dft_AE_relative, dftb_AE_relative, bolt_f):
        """
        Plot the energy figure between DFT and Optimized DFTB

        Parameters:


        """
        r0, r0_dens, c_rep, mu = parameters
        n_moles = int(len(dft) / 11) ; print('n_moles:', n_moles)

        d_min = self.dist_min(dft)
        print("len:", len(d_min), len(dft_AE_relative), len(dftb_AE_relative))

        line_name  = ['Dimer', 'Graphite', 'Diamond', '$\\mathrm{{\\beta}}$-Sn', 'BCC', 'FCC']
        plt.rcParams["figure.figsize"] = (5,5)
        basic_ms = 20
        for i in range(n_moles):
            if bolt_f[i] >= 0.1:
                plt.scatter(d_min[i*11:i*11+11], dft_AE_relative[i*11:i*11+11], marker='x',  linewidths=2, s= 10 * basic_ms * bolt_f[i], color=colors[i], alpha=0.8)
                plt.plot(d_min[i*11:i*11+11], dftb_AE_relative[i*11:i*11+11], marker='o', color=colors[i],  linestyle=linestyles[i], markersize=1.2 * basic_ms * bolt_f[i], alpha=0.7,  label=line_name[i])
                

            else:
                # plt.plot(d_min[i*11:i*11+11], dftb_AE_relative[i*11:i*11+11],color='#568203', markersize=0.5, linestyle='-',  linewidth=2.0, alpha=0.01)
                continue


        plt.ylabel('Rel. Energy (eV/atom)')
        plt.xlabel('$d_{\mathrm{min}}$ ($\mathrm{\AA}$)')
        plt.title(f"$r_0$={r0:.2f},$r_0^{{\mathrm{{d}}}}$={r0_dens:.2f},$c_{{\mathrm{{rep}}}}$={c_rep:.2f}, $\mu$={mu:.2e}")

        # plt.suptitle("{}".format(symbol))
        plt.legend()
        plt.tight_layout()
        # plt.title('Boltzmann factor', fontsize=fs)
        plt.savefig('dmin.png', dpi=200)
        print("Save dmin.png!!") 
        plt.clf()

    def plot_tradition(self, symbol, parameter_list, mu, dft_AE_relative, dftb_AE_relative, bolt_f):
        """
        Plot the energy figure between DFT and Optimized DFTB

        Parameters:
                symbol: chemical symbol, eg: 'C'
                parameter_list: the list of parameters, eg: [r_wave, r_dens, c_rep]
                mu: the loss function
                dft_AE_relative: the list of DFT relative energy per atom
                dftb_AE_relative: the list of Optimized DFTB relative energy per atom
                bolt_f: the list of Boltzmann factor of each configuration

        """

        n_moles = int(len(dft_AE_relative) / 11)
        r_wave, r_dens, c_rep, sigma = parameter_list

        large_font=10
        small_font=7
        X       = np.arange(80, 120.1, 4)
        config_list = ['dimer', 'graphite', 'diamond', 'beta-Sn', 'bcc', 'fcc']
        fig     = plt.figure(figsize=(6,6))
        plt.suptitle('Para_dftb:$r_w$={:0.3f} $r_d$={:0.3f} $c_{{rep}}$={:0.3f} loss={:.3e}'.format(r_wave, r_dens, c_rep, mu), fontsize=large_font)
        for p in range(0,n_moles):
            ax     = fig.add_subplot(math.ceil(n_moles/2), 2, p+1)
            Y_dftb = dftb_AE_relative[11*p : 11*(p+1)]
            Y_dft  = dft_AE_relative[11*p  : 11*(p+1)]
            if bolt_f[p] < 0.10:
                alpha=0.3
            else:
                alpha=1.0
            
            ax.scatter(X, Y_dft , marker='x', s=40, linewidths=1, color = '#0d3f67', \
                alpha=alpha, label = 'DFT_'  + symbol)
            ax.plot(X, Y_dftb, marker='o', markersize=5, linestyle='-', color = '#ca3e47',\
                    linewidth=0.8, alpha=alpha, label = 'DFTB_' + symbol)


            # Add annotation arrow to show min volume
            down = min(min(Y_dft, Y_dftb))
            up   = max(max(Y_dft, Y_dftb))
            len_of_arrow = (up - down) / 5
            end_coor, start_coor = (X[np.argmin(Y_dftb)], min(Y_dftb) + 0.5 * len_of_arrow), (X[np.argmin(Y_dftb)], min(Y_dftb) + 1.2 * len_of_arrow)
            plt.annotate('',xy=end_coor,xytext=start_coor,arrowprops=dict(arrowstyle="wedge",\
                         facecolor='#ca3e47'))


            # grad_dftb = gradients[11*p : 11*(p+1)]   # gradients
            # bx = ax.twinx()
            # bx.plot(X, grad_dftb, color = '#2694ab', linestyle=':', linewidth=1.0, alpha=alpha, label='2nd derivative')
            # bx.set_ylabel('2nd', fontsize=small_font-1, color='#2694ab')
            # bx.ticklabel_format(style='sci', scilimits=(-1,2), axis='y')
            # bx.tick_params(axis='y', labelsize=small_font-1, colors='#2694ab')

            ax.set_title(config_list[p] + ', bolt:' + str(round(bolt_f[p], 2)), fontsize=small_font, alpha=alpha)
            ax.tick_params(axis='both', labelsize=small_font)
            if bolt_f[p] < 0.10:
                plt.tick_params(axis='x',colors='grey')
                plt.tick_params(axis='y',colors='grey')
            if p > 3:
                ax.set_xlabel('Volume(%)',  alpha=alpha, fontsize=small_font)
                ax.set_xticks(np.arange(80, 120.1, 10))
            if p % 2 ==0:
                ax.set_ylabel('$\Delta$E/atom(eV)',  alpha=alpha, fontsize=small_font)
            if p == 0:
                ax.legend(loc='best',fontsize=small_font-1)
            else:
                continue
        plt.tight_layout()
        plt.savefig('dftb_ener.png', dpi=300)
        plt.close(fig)

        
